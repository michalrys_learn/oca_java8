package com.udemy1.classes3;

import com.relearn3.test1.overriding.B;
import com.relearn3.test1.overriding.Person;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

public class SomeOther2 {
    public static void main(String[] args) {
        Flyable[] test = new Flyable[2];
        System.out.println(Arrays.asList(test));
        for (Flyable element : test) {
            System.out.println(element.AGE);
        }

        Flyable bird = new Bird();
        System.out.println(bird.AGE);

        LocalDate parse1 = LocalDate.parse("2020-02-02");
        LocalDate parse2 = LocalDate.parse("2020-02-02");
        System.out.println(parse1.equals(parse2));
        System.out.println(parse1.isEqual(parse2));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yy");
        System.out.println(formatter.format(parse1));

        LocalDate years = LocalDate.of(1000, 1, 1);
        Period period = Period.ofYears(2000);
        System.out.println(years.minus(period));


        Object age = 100;
        System.out.println((Integer)age + 1);
    }
}

interface Flyable {
    int AGE = 10;
}

class Bird implements Flyable {
    int AGE = 50;
}
