package com.udemy1.classes3;

class A {

    String name = "A";

    void print() {
        System.out.println("A");
    }


    void onlyInA() {
        System.out.println("only in A");
        print();
    }

}

class B extends A {
    String name = "B";

    void print() {
        System.out.println("B");
    }

    void onlyInB() {
        System.out.println("only in B");
    }
}

class C extends B {
    String name = "C";

    void print() {
        System.out.println("C");
    }

    void onlyInC() {
        System.out.println("only in C");
    }

}

public class PolyTest1 {
    public static void main(String[] args) {
        A c = new C();
        ((A) (B) c).onlyInA();
        ((A) (B) c).print();
        System.out.println(((A) (B) c).name);
        System.out.println("==");
        c.onlyInA();
        System.out.println("==");

        System.out.println("------");

        C c2 = new C();
        ((A) (B) c2).onlyInA();
        ((A) (B) c2).print();
        System.out.println(((A) (B) c2).name);


    }
}
