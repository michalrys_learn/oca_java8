package com.udemy1.classes3.a1_test;

class A {
    public int age = 10;

    A() {
        System.out.println("sdf");
    }

    protected void printMe() {
        System.out.println("A");
    }

    void printMeAgain() {
        System.out.println("A");
    }

    public int getAge() {
        return age;
    }
}

class B extends A implements C{
    public int age = 100;

    public void printMe() {
        System.out.println("B");
    }

    @Override
    public int getAge() {
        return age;
    }

    public void printMeC() {

    }

    protected void printMeAgain() {
        System.out.println("B");
    }

    public static void main(String[] args) {
        A a_b = new B();
        a_b.printMe();
        a_b.printMeAgain();

        B b = new B();
        A a = (A) b;
        a.printMe();

        System.out.println(a_b.age);
        System.out.println(a_b.getAge());

    }
}

interface C {
    void printMeC();
}