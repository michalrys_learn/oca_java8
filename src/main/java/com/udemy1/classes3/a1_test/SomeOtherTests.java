package com.udemy1.classes3.a1_test;

public class SomeOtherTests {
    public static Object msg = "TEST";
    public static Object age = 123.0;

    public static void main(String[] args) {
        System.out.println("ala ma kota " + msg);
        System.out.println(msg);

        System.out.println("1" + age);
//        System.out.println(1 + (int)age);
        System.out.println(1.0 + (double) age);

        System.out.println("----");
        X z = new Z();
        z.print();

        Z z2 = new Z();
        System.out.println(z2.a);
        z.staticMethod();
        System.out.println(z.name);

        Ia ia = null;
        System.out.println(ia.TYPE);
    }
}

class X {
    int a = 1;
    static String name = "static name";

    public void print() {
        System.out.println("X");
    }

    public static void staticMethod() {
        System.out.println("static method");
    }
}

class Y extends X {
    public void print() {
        System.out.println("Y");
    }
}

class Z extends Y {
    public void print() {
        System.out.println("Z");

    }
}

interface Ia {
    String TYPE = "A";
}

interface Ib extends Ia {
}
