package com.udemy1.classes3.a1_test;

public class Test1 {
    private static void add(double d1, double d2) {
        System.out.println("double version: " + (d1 + d2));
    }

    private static void sub(Double d1, Double d2) {
        System.out.println("Double version - sub.");
    }

    static void print(double a) {
        System.out.println("OK double");
    }

    static void print(Double a) {
        System.out.println("OK Double");
    }

    public static void main(String[] args) {
        System.out.println(new Integer(2).getClass());

        add(10.0, new Integer(10));
//        sub(2.0, 2);
        print(new Integer(3));
        print(1);

    }
}
