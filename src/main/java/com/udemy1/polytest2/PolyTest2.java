package com.udemy1.polytest2;

class A {
    void uniqueForA() {
        System.out.println("unique method for A");
        print();
    }

    void print() {
        System.out.println("print in A");
    }
}

class B extends A {
    void print() {
        System.out.println("print in B");
    }
}

public class PolyTest2 {
    public static void main(String[] args) {
        A b = new B();
        b.uniqueForA();
    }
}
