package com.udemy1.basic1.a1_basics;

public class Other3 {
    public static void main(String[] args) {
        final boolean flag;
        flag = false;
        while (flag) {
            System.out.println("error");
        }

        for (int i = 0; flag; i++) {
            System.out.println("error");
        }

        do {
            System.out.println("hello");
        } while (flag);
        System.out.println("sdf");

        while (true) {
            System.out.println("sdf");
            if (2 + 2 > 5) {
                break;
            }
        }
        System.out.println("sdf");
    }
}
