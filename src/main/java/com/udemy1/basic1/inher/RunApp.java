package com.udemy1.basic1.inher;

public class RunApp {
    public static void main(String[] args) {
        Human obj = new Person();
        System.out.println(obj instanceof Human);
        System.out.println(obj instanceof Person);
        System.out.println(obj instanceof Student);

    }
}
