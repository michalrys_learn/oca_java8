package com.udemy1.exception2;

import com.udemy1.basic1.inher.Student;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

class A {
    public static void print() throws IOException {
        throw new FileNotFoundException();
    }

    public static void main(String[] args) {
        try {
            print();
        } catch (FileNotFoundException exception) {
            System.out.println("file not found");
        } catch (IOException e) {
            System.out.println("io exc");
        }
    }
}

class B {
    public static void main(String[] args) {
        System.out.println(1);
    }
}

class C {
    C() throws Exception {
        System.out.println("constr");
    }

    private static void print() throws Exception {
        System.out.println("hello");
    }

    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
//        System.out.println(sb.append(null));
        System.out.println(Boolean.valueOf(null));
        System.out.println(" ".isEmpty());

        try {
            print();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

class D {
    public static void main(String[] args) {
        System.out.println("\n".isEmpty());
        System.out.println("\r".isEmpty());
        System.out.println("\t".isEmpty());
        System.out.println(" ".isEmpty());
        System.out.println("".isEmpty());
        System.out.println("d".contentEquals("d"));

        StringBuilder sb = new StringBuilder();
        sb.replace(0, 1, "ala");
        "ala".replace("a", "o");

        System.out.println(null + "ad");
        long value = 2;
    }
}

class E {
    public static void main(String[] args) {
        List<String> values = new ArrayList<>();
        values.add("ala");
        values.add("ola");
        values.add("ala");
        Iterator<String> iterator = values.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().startsWith("a")) {
                iterator.remove();
            }
        }
        System.out.println(values);

        System.out.println(new StringBuilder("ala").equals(new StringBuilder("ala")));
    }
}

class F {
    public static void main(String[] args) {
        List<String> values = new ArrayList<>(4);
        values.add(0, "ala");
        values.add(1, "ola");
        System.out.println(values);

        List<Integer> numbers = new ArrayList<>();
        numbers.add(100);
        numbers.add(200);
        numbers.add(100);
        numbers.remove(Integer.valueOf(100));
        System.out.println(numbers);
    }
}

class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }

    public boolean equals(Person obj) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;

        if(obj instanceof Person) {
            Person stud = (Person)obj;
            if(this.name.equals(stud.name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}

class G {
    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>();
        persons.add(new Person("ala"));
        persons.add(new Person("ola"));
        persons.add(new Person("ala"));

        persons.remove(new Person("ala"));
        System.out.println(persons);

    }
}









