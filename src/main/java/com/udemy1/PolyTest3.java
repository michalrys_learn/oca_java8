package com.udemy1;

interface I1 {
    int AGE = 3;

    static void print() {
        System.out.println("hello");
    }
}

interface I2 {
    int AGE = 30;
}

class A implements I1, I2 {
}

public class PolyTest3 {
    public static void main(String[] args) {
        A a = new A();
        System.out.println(I1.AGE);
        System.out.println(I1.AGE);
        I1.print();
    }
}
