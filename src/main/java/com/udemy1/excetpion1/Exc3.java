package com.udemy1.excetpion1;

import java.io.FileNotFoundException;
import java.io.IOException;

class A {
    public A() throws IOException {
        System.out.println(1);
    }
}

class B extends A {
    public B() throws FileNotFoundException, IOException {
        System.out.println(2);
    }
}

public class Exc3 {
    public static void main(String[] args) throws Exception {
        new B();

        try {
            System.out.println("test");
        } catch (Exception e) {
            System.out.println("some exception");
        }

    }
}
