package com.udemy1.excetpion1;

import java.sql.SQLException;

class Test {
    public static void print() throws SQLException {
        try {
            throw new SQLException();
//            Exception sql = new SQLException("sql");
//            throw sql;
        } catch (Exception e) {
            System.out.println(e);
//            e = null;
            throw e;
        }
    }
}

class Test2 {
    public static void print2() throws SQLException {
        try {
            throw new SQLException();
        } catch (Exception e) {
            throw null;
        }
    }
}

class Test3 {
    public Test3() throws Exception {
        throw new Exception();
    }
}

public class Exc2 {
    public static void main(String[] args) {
        try {
            Test.print();
        } catch (SQLException e) {
            System.out.println(e);
        }

        System.out.println("--2---");
        try {
            Test2.print2();
        } catch (SQLException e) {
            System.out.println("sql");
        } catch (RuntimeException e) {
            System.out.println("null pointer");
        }

        System.out.println("---3---");
        try {
            Test3 test3 = new Test3();
        } catch (Exception e) {
            System.out.println("exception in constructor");
        }
    }
}
