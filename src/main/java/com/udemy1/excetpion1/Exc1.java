package com.udemy1.excetpion1;

public class Exc1 {
    public static void print() throws Exception {
        System.out.println("test");
        throw new Exception();
    }

    public static void main(String[] args) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append("123");
        sb.delete(0, 100);

        try {
            throw null;
        } catch (NullPointerException e) {
            System.out.println("null");
        }
    }
}
