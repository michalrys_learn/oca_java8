package com.relearn5.a;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


class A {
    public static void main(String[] args) {
        String[] names = {"ala", "tola", "ula"};
        List<String> list = Arrays.asList(names);

        names[0] = "Alicja";
        System.out.println(list);
        System.out.println(Arrays.toString(names));
//        list.remove("ala");
//        System.out.println(list);



        System.out.println("------");

        List<String> values = new ArrayList<>();
        values.add("test1");
        values.add("test2");
        values.add("test3");

        String[] array = values.toArray(new String[0]);
        values.remove("test3");
        array[0] = "ala ma kota";
        System.out.println(Arrays.toString(array));
        System.out.println(values);

    }
}

class B {
    public static void main(String[] args) {
        boolean flag1 = true;
        final boolean flag2 = false;
        if(flag1) {
            System.out.print(1); //Line n1
        }
        if(flag1 | flag2) {
            System.out.print(2); //Line n2
        }
        if(flag2) {
            System.out.print(3); //Line n3
        }
        if(false) {
            System.out.print(4); //Line n4
        }
        System.out.print(5); //Line n5

    }
}


class C {

}









