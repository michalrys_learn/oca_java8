package com.relearn4.chap5;

import java.io.IOException;

class A {
    public static final String TYPE = "A";

    public static void print() {
        System.out.println("A");
    }
}

class B extends A {
    public static final String TYPE = "B";

    public final static void print() {
        System.out.println("B");
    }

    public static void main(String[] args) {
        print();
        A ab = new B();
        ab.print();
        ((B) ab).print();

    }
}

abstract class C {
    protected abstract void print();
}

class D extends C {
    @Override
    public void print() {

    }
}

interface Runnable {

}

interface Movable {
}

interface Actionable extends Runnable, Movable {
}

class E {
    public static void main(String[] args) {
        try {
            throw new IOException();
        } catch (Exception e) {
            System.out.println("IT IS OK");
        } finally {
            System.out.println("zonk");
        }
    }
}

class F {
    public static void main(String[] args) {
        try {
            throw new Exception("1");
        } catch (Exception e) {
            throw new RuntimeException("caught");
        } finally {
            System.out.println("finally");
        }
//        System.out.println("next");
    }
}

class G {
    public G() throws Exception {
    }

    public static void main(String[] args) throws Exception {
        try {
            G g = new G();
            throw null;
        } catch (RuntimeException e2) {
            System.out.println("RTE");
        } catch (Exception e) {
            System.out.println("ok");
            throw new Exception("new Exc");
        }

    }
}










