package com.relearn4.chap5.b;

class A {
    public static String a = "sd";

    public static void main(String[] args) {
        String a = A.a + "x";

        A empty = (A) null;
        System.out.println(empty.a);
    }
}

interface I1 {
    String NAME = "super";
}

interface I2 extends I1 {
    String NAME = "not usper";
}

interface I3 extends I1, I2 {
}

class B implements I2 {
    String NAME = "b";

    public static void main(String[] args) {
        B b = new B();
        System.out.println(b.NAME);
        I1 bInterface = new B();
        System.out.println(bInterface.NAME);
        System.out.println(((B)bInterface).NAME);

    }
}