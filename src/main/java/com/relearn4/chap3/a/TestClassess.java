package com.relearn4.chap3.a;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class A {
    public static void main(String[] args) {
        System.out.println("hello world");
        char[] c = {'a', 'b'};
        int[] i = {1, 2};

        i[0] = 'd';
        c[0] = 23;
        System.out.println(Arrays.toString(i));
        System.out.println(Arrays.toString(c));

        Object[] elements = {"aa", "bb"};
        String[] texts = {"1"};
        elements = texts;
        System.out.println(elements[0]);

        System.out.println(new Boolean(null));

    }
}

class B {
    public static void main(String[] args) {
        final String a = "ala";
        final String b = " ma";
        System.out.println(a + b == "ala" + " ma");

        StringBuilder sb = new StringBuilder();

        System.out.println("ala" + null);

        System.out.println("MISSSSSSS".replace("SS", "T"));

        System.out.println("alatola".indexOf("a", 2));

        printMe(null);

        System.out.println("  ".isEmpty());
    }

    private static void printMe(Object text) {
        System.out.println("Object");
    }

    private static void printMe(String text) {
        System.out.println("String");
    }
}

class C {
    public static void main(String[] args) {
        System.out.println(new Boolean(null));
        Double aDouble = new Double(2.7);
        System.out.println(aDouble.intValue());


        boolean a = 3 > 4 ? 4 > 5 ? true : false : false;
    }
}

class D {
    public static void main(String[] args) {
        int x = 3;
        while (x > 0)

            System.out.println(--x);

        System.out.println("Finito");
    }
}

class E {
    public static void main(String[] args) {
        int i = 5;
        while (true) {
            System.out.println(i--);
            if (i == 3) {
                break;
            } else {
                System.out.println("oh no");
                continue;
            }
//            System.out.println("ala kota");
        }
        System.out.println("test");

    }
}

class F {
    public static void main(String[] args) {
        System.out.println(args.length);
        int[] values = new int[3];
        int amount = values.length;
        System.out.println(amount);
        System.out.println(values.length);
    }
}

class G {
    public static void main(String[] args) {
        List<String> a = new ArrayList<>();
        List<String> b = new ArrayList<>();
        a.add("ala");
        a.add("ola");
        b.add("ala");
        b.add(new String("ola"));
        System.out.println(a.equals(b));

        List<Integer> a2 = new ArrayList<>();
        List<Integer> b2 = new ArrayList<>();
        a2.add(1);
        a2.add(2);
        a2.add(3);
        b2.add(1);
        b2.add(2);
        b2.add(3);
        System.out.println(a2.equals(b2));

    }
}