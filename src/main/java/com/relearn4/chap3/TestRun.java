package com.relearn4.chap3;


import sun.util.resources.LocaleData;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class TestRun {
    public static void main(String[] args) {
        String text = "ala ma kota";
        System.out.println(text.substring(5));
        System.out.println(">" + text.substring(5, 5) + "<  ");
        System.out.println(text.substring(5, 5).length());

        StringBuilder sb = new StringBuilder();
        sb.append("ala ma kota\n");
//        System.out.println(sb.deleteCharAt(sb.length() - 1));
        System.out.println(sb);
        System.out.println("--");

        System.out.println(sb.replace(4, 6, "p"));

        int[][] a = new int[2][];
        a = new int[4][1];
        int[][] a2 = new int[2][1];
        a2 = new int[4][4];
        a2 = new int[][]{{}, {2}};

        int[][] p = new int[3][3];
        p[0][0] = 100;
        p[2][2] = p[0][0];
        p[0][0] = 200;
        for (int[] inner : p) {
            System.out.println(Arrays.toString(inner));
        }

        String[][] t = new String[3][3];
        String aa = "100";
        t[0][0] = aa;
        t[2][2] = t[0][0];
        aa = "200";
        for (String[] inner : t) {
            System.out.println(Arrays.toString(inner));
        }

        List<String> values = new ArrayList<>();
        values.add("test");
        values.add("test1");
        values.add("test2");
        values.add("test");
        System.out.println(values.contains("test2"));
        System.out.println(values.subList(0,2));
        values.add(1, "ala");
        System.out.println(values);

        String[] array = values.toArray(new String[1]);
        values.add("tes");
        System.out.println(values);

        Integer integer = Integer.valueOf(1);
        int i = Integer.parseInt("1");

        System.out.println(Boolean.parseBoolean(null));
        System.out.println(Boolean.valueOf("sdf"));

        LocalTime myTime = LocalTime.of(12, 0, 0);
        myTime = myTime.minusHours(2).plusMinutes(20).plusSeconds(50);
        System.out.println(myTime.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM)));

        System.out.println(Period.ofDays(100).ofYears(1));
    }
}
