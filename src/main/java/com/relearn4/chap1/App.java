package com.relearn4.chap1;

import com.relearn2.chapter5_classes.f2_poly.B;

import java.util.Arrays;

class A1 {
}

class B1 extends A1 {
}

class C1 extends B1 {
}


class App {
    public static void main(String[] args) {
//        dataTypes();
//        initializations();
//        instaceOfCheck();
//        ternaryOperators();
//        infitiniveLoops();

        int i = 10;
        i = 100 + i++;
        System.out.println(i);

        int b = 1;
        b = b++ + --b;
        System.out.println(b);

        int c = 0;
        System.out.println(++c + ++c >= ++c + ++c & c > 0 | c > 0);


    }

    private static void infitiniveLoops() {
//        do;while(true);
//        do;while(false);
    }

    private static void ternaryOperators() {
        A1 results = 2 > 1 ? new B1() : new C1();
        System.out.println(results);

        Number value = 2 > 1 ? new Integer(2) : new Double(2.0);
        System.out.println(value);
    }

    private static void instaceOfCheck() {
        int x = 129;
        byte y = 1;

        y += x;
        System.out.println(y);

        String text = "ala";
        Object object = new Object();
        Integer number = new Integer(3);

        System.out.println(text instanceof Object);
        System.out.println(number instanceof Number);
        System.out.println(number instanceof Object);
        System.out.println(number instanceof Integer);
    }

    private static void initializations() {
        int value1 = 0123;
        int value2 = 0121;
        System.out.println(value1 + value2);
        System.out.println(0123 + 0121);
        int binary1 = 0b0111_1111;

        int[] a, b, c = {1, 2, 3}, d[] = {{}, {2, 3}, {1, 2}};

        int[] arr = new int[3];
        System.out.println(Arrays.toString(arr));
    }

    private static void dataTypes() {
        int a1 = 1_0;
        long a2 = 1_0L;
        double b1 = 1.0;
        float b2 = 1.0f;

        System.out.printf("int=%d, long=%d, double=%f, float=%f\n", a1, a2, b1, b2);
        System.out.println(a1);
        System.out.println(a2);
        System.out.println(a1 == a2);
        System.out.println(b1);
        System.out.println(b2);
        System.out.println(b1 == b2);
    }
}
