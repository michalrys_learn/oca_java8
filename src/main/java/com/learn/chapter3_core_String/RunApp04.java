package com.learn.chapter3_core_String;

public class RunApp04 {
    public static void main(String[] args) {
        stringTest01();
        stringTest02();

        stringBuilderTest01();

    }

    private static void stringBuilderTest01() {
        StringBuilder words = new StringBuilder();
        System.out.println("---");
        words.append("ala ma kota\n");
        words.append("ola ma psa\n");
        words.deleteCharAt(words.length() - 1);
        System.out.println(words);
        System.out.println("---");


    }

    private static void stringTest02() {
        System.out.println("--------------");
        System.out.println("Test 02: url parsing");
        String request = "http://www.onet.pl/?name=Mike&title=some&id=12312";
        System.out.printf("\t%s\n", request);
        int from = request.indexOf("name=") + "name=".length();
        int to = request.indexOf("&", from + 1);
        String name = request.substring(from, to);
        System.out.printf("\tName from request is '%s'.\n", name);
    }

    private static void stringTest01() {
        System.out.println("Test 01: String indexOf");
        String a = "ala ma kota, ala też ma psa, ale ala już nie ma papugi";
        System.out.printf("\t%s\n", a);
        int amount = 0;
        int id = -1;
        while ((id = a.indexOf("ala", ++id)) != -1) {
            System.out.printf("\t%d",id);
            amount++;
        }
        System.out.printf("\n\tWord 'ala' occurs %d times.\n", amount);
    }
}
