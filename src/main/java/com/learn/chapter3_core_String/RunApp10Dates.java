package com.learn.chapter3_core_String;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class RunApp10Dates {
    public static void main(String[] args) {
        //tests1();
        //tests2();
        testA();
        testB();

        printName("ala");
        printName("ola");
    }

    private static void printName(String name) {
        if (name.equals("ala")) {
            return;
        }
        System.out.println(name);
    }

    private static final void testA() {
        System.out.println("my test A");
    }

    private final synchronized static void testB() {
        System.out.println("other test");
    }

    private static void tests2() {
        List<String> names = Arrays.asList();
        System.out.println(names);
        names.set(0, "michal");
        System.out.println(names);
    }

    private static void tests1() {
        LocalDate myDate = LocalDate.of(2023, Month.FEBRUARY, Month.FEBRUARY.maxLength() - 2);
        LocalTime myTime = LocalTime.of(23, 34);
        LocalDateTime myDateTime = LocalDateTime.of(myDate, myTime);

        myDateTime = myDateTime.plusDays(23).plusHours(2).plusSeconds(23);

        System.out.println(myDateTime);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YY-MMM-dd HH-mm-ss", Locale.UK);
        System.out.println(myDateTime.format(formatter));

        Period period = Period.ofDays(20);
        Duration duration = Duration.of(3_000, ChronoUnit.SECONDS);
        System.out.println(myDate.plus(period));
        System.out.println(myTime.format(DateTimeFormatter.ISO_TIME));
        System.out.println(myTime.plus(duration).format(DateTimeFormatter.ISO_TIME));

        DateTimeFormatter f = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
        System.out.println(myDateTime.format(f));

        ArrayList<String> namesA = new ArrayList<>();
        ArrayList<String> namesB = new ArrayList<>();
        ArrayList<String> namesC = new ArrayList<>();
        ArrayList<String> namesD = new ArrayList<>();
        ArrayList<String> namesE = new ArrayList<>();

        namesA.add("ala");
        namesA.add("ola");
        namesA.add("ula");

        namesB.add("ula");
        namesB.add("ala");
        namesB.add(new String("ola"));

        namesC.add("ala");
        namesC.add(new String("ola"));
        namesC.add("ula");

        System.out.println(namesA.equals(namesB));
        System.out.println(namesA.equals(namesC));

        List<Integer> values = Arrays.asList(1, -3, 2, 6);
        Collections.sort(values);
        System.out.println(values);
        values.set(0, 3);
        System.out.println(values);
    }
}
