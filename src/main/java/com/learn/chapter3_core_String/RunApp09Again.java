package com.learn.chapter3_core_String;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RunApp09Again {
    public static void main(String[] args) {
        stringAndSB1();
        arraysTests1();

        int[][] a = new int[3][];

        int i = Integer.parseInt("1");
        Integer integer = Integer.valueOf("1");

        List<String> aa = new ArrayList<>();
        System.out.println(aa.indexOf("ala"));

        Integer[] tabValues = {1, 2, 3, 4};
        List<Integer> listValues = Arrays.asList(tabValues);
        System.out.println(listValues);

        List<String> names = Arrays.asList("12", "ds");
        String[] strings = names.toArray(new String[0]);
        System.out.println(Arrays.toString(strings));


    }

    private static void arraysTests1() {
        int[] a = new int[]{1, 2, 3};
        int[] b = {1, 2, 3};
        int[] c = new int[3];
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(b));
        System.out.println(Arrays.toString(c));

        int[] x, y, z[];
        z = new int[3][];
        z[0] = new int[1];
        z[1] = new int[5];
        z[2] = new int[2];

        String[] txt = new String[2];
        txt[0] = "ala";
        txt[1] = new StringBuffer("test").toString();
        System.out.println(Arrays.toString(txt));

        Object[] test2 = new Object[3];
        test2[0] = "ala ma kota";
        test2[1] = new StringBuilder("test");
        test2[2] = new Integer(2);
        System.out.println(Arrays.toString(test2));
    }

    private static void stringAndSB1() {
        String txt = "ala";

        System.out.println(txt.indexOf("b", 10));
        System.out.println(txt.substring(2, 2));

        String txt2 = new String("ala");
        System.out.println("ala".equals(txt));
        System.out.println("ala" == txt);

        StringBuilder sb = new StringBuilder();
        sb.append("ala ma kota");
        sb.insert(sb.indexOf("kota"), "psa i ");
        sb.insert(sb.length(), "!");
        System.out.println(sb);
        System.out.println("ala" == "a" + "la");


        StringBuilder sb2 = new StringBuilder("ala");
        StringBuilder sb3 = new StringBuilder("ala");
        System.out.println(sb2.equals(sb3));
    }
}
