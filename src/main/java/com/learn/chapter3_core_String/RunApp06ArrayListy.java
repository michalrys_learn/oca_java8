package com.learn.chapter3_core_String;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RunApp06ArrayListy {
    public static void main(String[] args) {
        //arrayListTests01();
        //arrayListTests02();

        //arrayListTests03ListToArray();
        int[] valuesB = {1, 2, 5, 8};
        List<int[]> ints = Arrays.asList(valuesB);

        Integer[] values = {1, 2, 5, 8};
        List<Integer> integers = Arrays.asList(values);
        System.out.println(Arrays.toString(values));
        System.out.println(integers);

        //integers.remove(0);
        values[1] = 100;

        System.out.println(Arrays.toString(values));
        System.out.println(integers);

        List<String> people = Arrays.asList("ula", "aa", "xnenon", "ala", "ma", "ola", "xla", "aa", "am", "kota");
        System.out.println(people);

//        Collections.sort(people, Collections.reverseOrder());
        System.out.println(people);
        Collections.sort(people, (x, y) -> {
            if (x.length() > y.length()) {
                return 2;
            } else if (x.length() < y.length()) {
                return -2;
            } else {
                return x.compareTo(y);
            }
        });
        System.out.println(people);

    }

    private static void arrayListTests03ListToArray() {
        List<String> persons = new ArrayList<>();
        persons.add("Ala");
        persons.add("Ola");
        persons.add("Ula");

        Object[] objects = persons.toArray();
        System.out.println(Arrays.toString(objects));

        String[] strings = persons.toArray(new String[5]);
        System.out.println(Arrays.toString(strings));

        String[] myPersons = new String[5];
        myPersons[0] = "Kot";
        String[] strings1 = persons.toArray(myPersons);
        System.out.println(Arrays.toString(strings1));
    }

    private static void arrayListTests02() {
        List<String> test = new ArrayList<>(10);
        System.out.println(test);
        test.add("Ala");
        //test.set(2, "Michal");
        test.add("test");
        test.add("ma");
        test.add("psa");
        System.out.println(test.size());
        test.set(2, null);
        System.out.println(test);
        System.out.println(test.size());

        Byte b = new Byte((byte) 1);
        Long L = new Long(1L);
    }

    private static void arrayListTests01() {
        List<String> b = new ArrayList<>(Arrays.asList("2", "32", "adsf"));
        ArrayList a = new ArrayList(b);

        System.out.println(a);

        b.remove("32");
        System.out.println(a);
        System.out.println(b);
        String remove = b.remove(0);
        System.out.println(remove);
        System.out.println(b);

        b.add("ala");
        b.add("ala");
        b.add("ala");
        b.add("ala");
        b.add("xx");
        System.out.println(b);
        b.remove("alass");
        System.out.println(b.remove("alass"));
        System.out.println(b);
    }
}
