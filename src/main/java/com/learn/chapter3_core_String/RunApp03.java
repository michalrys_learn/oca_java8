package com.learn.chapter3_core_String;

public class RunApp03 {
    public static void main(String[] args) {
        String txt = "animal";
        System.out.println(txt.replace('a', 'A'));
        System.out.println(txt.replace("a", "A"));

        System.out.println("AniMaL ".trim().toLowerCase().replace('a', 'A'));

        StringBuilder results = new StringBuilder();
        results.append("ala");
        results.append(" ma ");
        results.append("kota\n");
        results.deleteCharAt(results.length() - 1);

        System.out.println("--");
        System.out.println(results);
        System.out.println("--");

    }
}
