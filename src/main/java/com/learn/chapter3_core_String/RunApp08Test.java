package com.learn.chapter3_core_String;

public class RunApp08Test {
    public static void main(String[] args) {
        //testA();

        String a = "";
        a += 2;
        a += 'c';
        a += false;
        if (a == "2cfalse") System.out.println("==");
        if (a.equals("2cfalse")) System.out.println("equals");

        String b = "ala";
        String c = "ala";
        b = "ala" + "test";
        c += "test";
        System.out.println("alatest" == b);
        System.out.println("alatest" == c);

    }

    private static void testA() {
        String a = "java";
        String x = "java2";
        StringBuilder b = new StringBuilder("java");

        System.out.println(a == x);

        Double a2 = 2.0;
        Float a3 = 1.0f;
        Double a4 = 2.0;
    }
}
