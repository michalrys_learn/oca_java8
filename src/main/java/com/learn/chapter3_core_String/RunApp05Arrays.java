package com.learn.chapter3_core_String;

import java.util.Arrays;

public class RunApp05Arrays {
    public static void main(String[] args) {
        //arraysTests01();
        //arraysTest02();

        System.out.println("Multi dimentional asymetric arrays");
        int[][] a1 = new int[3][];
        a1[0] = new int[3];
        a1[1] = new int[2];
        a1[2] = new int[4];


        System.out.println("\tfirst way");
        for (int i = 0; i < a1.length; i++) {
            System.out.printf("\t[");
            for (int j : a1[i]) {
                System.out.printf(" %d ", a1[i][j]);
            }
            System.out.printf("]\n");
        }


        System.out.println("\tsecond way");
        for (int [] x : a1) {
            System.out.printf("\t[");
            for (int y : x) {
                System.out.printf(" %d ", y);;
            }
            System.out.printf("]\n");
        }
    }

    private static void arraysTest02() {
        int[] a = {3, 1, 3};
        int[] b = {3, 1, 3};
        int[] c = {3, 1};
        int[] d = a;

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);

        System.out.println(a == d);
        System.out.println(a == b);
        System.out.println(a.equals(b));

        String[] t1 = {"ala"};
        String[] t2 = {"ala"};
        System.out.println(t1.equals(t2));
        System.out.println(Arrays.toString(t1));

        t2[0] = t1[0];
        System.out.println(t1.equals(t2));
        System.out.println(t1);
        System.out.println(t2);
        System.out.println(t1[0].equals(t2[0]));

        String[] strings = {"stringValue"};
        Object[] objects = strings;
        String[] againStrings = (String[]) objects;
        //againStrings[0] = new StringBuilder(); // DOES NOT COMPILE
        StringBuilder sb = new StringBuilder();
        objects[0] = sb.toString(); // careful!

        Arrays.sort(a);
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.binarySearch(a, 0));
    }

    private static void arraysTests01() {
        char[] letters;
        letters = new char[3];
        System.out.println(letters[0]);
        letters[1] = 'O';
        for (char letter : letters) {
            System.out.println(letter);
        }
        System.out.println(Arrays.toString(letters));

        String[] words = new String[3];
        System.out.println(Arrays.asList(words));

        int[] numbers = new int[3];
        for (int number : numbers) {
            System.out.println(number);
        }

        double[] values = new double[3];
        for (double value : values) {
            System.out.println(value);
        }

        String[] t = new String[]{"ala", "ma", "kota"};
        String t2[] = new String[]{"ala", "ma", "kota"};
        String t3[] = new String[]{"ala", "ma", "kota"};
        String[] t4 = {"ala", "ola"};
        int[] t5 = {1, 2, 3};

        int a1[], a2, a3[];
        int[] b1, b2;
    }
}
