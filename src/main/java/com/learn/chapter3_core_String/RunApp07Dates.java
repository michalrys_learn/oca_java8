package com.learn.chapter3_core_String;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;

public class RunApp07Dates {
    public static void main(String[] args) {

        LocalDate dateA = LocalDate.of(2023, Month.DECEMBER, 24);

        Period period = Period.ofDays(2);
        Duration duration = Duration.ofHours(3);
        LocalTime now = LocalTime.now();
        now = now.plus(duration);
        System.out.println(now);

        LocalDateTime today = LocalDateTime.now();
        today = today.plus(duration);
        System.out.println(today);

    }
}
