package com.learn.chapter3_core_String;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class RunApp01 {
    public static void main(String[] args) {
        String txt = "Ala ma kota, ola tez ma kota, o popatrz, konstantynopolka, a ja mam psa.";

        System.out.println(txt.indexOf("ma", 4));
        System.out.println(txt.indexOf("a", txt.length() - 1));
        System.out.println("----------");
        int id = -1;
        while ((id = txt.indexOf("a", ++id)) != -1) {
            System.out.println(id);
        }

        char[] chars = txt.toCharArray();
        Set<Character> uniqueLetters = new TreeSet<>();
        for (char c : chars) {
            if (c == ' ' || c == ',' || c == '.') {
                continue;
            }
            uniqueLetters.add(c);
        }

        Map<Character, Integer> letterAmount = new TreeMap<>();
        for (Character letter : uniqueLetters) {
            id = -1;
            while ((id = txt.indexOf(letter, ++id)) != -1) {
                int amount = letterAmount.getOrDefault(letter, 0);
                letterAmount.put(letter, ++amount);
            }
        }

        System.out.println(letterAmount);

        Map<Integer, String> amountLetter = new TreeMap<>(Collections.reverseOrder());
        for (char letter : letterAmount.keySet()) {
            int amount = letterAmount.get(letter);
            String letters = amountLetter.getOrDefault(amount, "");
            amountLetter.put(amount, letters + letter);
        }
        System.out.println(amountLetter);

        txt = txt.replaceAll("[,.]", "").trim().toLowerCase(Locale.ROOT);
        Set<String> words = new TreeSet<>(Arrays.asList(txt.split(" ")));
        System.out.println(words);

        Map<Integer, List<String>> wordsSorted = new TreeMap<>(Collections.reverseOrder());
        for (String word : words) {
            List<String> currentWords = wordsSorted.getOrDefault(word.length(), new LinkedList<>());
            currentWords.add(word);
            wordsSorted.put(word.length(), currentWords);
        }
        System.out.println(wordsSorted);
        LinkedList<Integer> amounts = new LinkedList<>(wordsSorted.keySet());
        List<String> longestWords = wordsSorted.get(amounts.get(0));
        List<String> shortestWords = wordsSorted.get(amounts.get(amounts.size() - 1));
        System.out.println(longestWords);
        System.out.println(shortestWords);


        String substring = txt.substring(0, 5);


        String alaWithOrWithoutCat = txt.substring(txt.toLowerCase().indexOf("ala"),
                txt.toLowerCase(Locale.ROOT).indexOf("kota") + "kota".length());
        System.out.println(alaWithOrWithoutCat);

        String a = "ala";
        String b = "Ala";
        System.out.println(a.equalsIgnoreCase(b));

        System.out.println("AlaMaKota".replace("a", "o"));
        System.out.println("AlaMaKota".replace("a", "o").trim());
    }
}
