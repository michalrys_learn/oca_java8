package com.learn.chapter5_classes.c2_inheritance_method_hiding;

public class RunApp {
    public static void main(String[] args) {
        Animal animal = new Animal();
        animal.getIsOk();
        System.out.println(animal.isOK());

        Cat cat = new Cat();
        cat.getIsOk();
        System.out.println(cat.isOK());

        Animal catPoly = new Cat();
        catPoly.getIsOk();
        System.out.println(catPoly.isOK());
    }
}
