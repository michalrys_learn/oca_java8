package com.learn.chapter5_classes.c2_inheritance_method_hiding;

public class Animal {
     public static boolean isOK() {
        return true;
    }

    public void getIsOk() {
        System.out.println(isOK());
    }
}
