package com.learn.chapter5_classes.c2_inheritance_method_hiding;

public class Cat extends Animal {
    public static boolean isOK() {
        return false;
    }

    public void getIsOk() {
        System.out.println(isOK());
    }
}
