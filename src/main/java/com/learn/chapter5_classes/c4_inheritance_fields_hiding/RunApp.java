package com.learn.chapter5_classes.c4_inheritance_fields_hiding;

public class RunApp {
    public static void main(String[] args) {
        Student student = new Student();
        student.printAge();
        student.setAge(22);
        student.setAge(23);
        student.printAge();

        Person personStudent = new Student();
        personStudent.printAge();
        personStudent.setAge(20);
        personStudent.setAge(50);
        personStudent.printAge();

    }
}
