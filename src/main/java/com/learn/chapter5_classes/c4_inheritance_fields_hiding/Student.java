package com.learn.chapter5_classes.c4_inheritance_fields_hiding;

public class Student extends Person {
    private int age;

    @Override
    public void setAge(int age) {
        this.age = age;
        super.age++;
    }

    @Override
    public void printAge() {
        System.out.printf("Age -> person = %d, student = %d.\n", super.age, this.age);
    }
}
