package com.learn.chapter5_classes.c4_inheritance_fields_hiding;

public class Person {
    protected int age;

    public void setAge(int age) {
        this.age = age;
    }

    public void printAge() {
        System.out.println(age);
    }
}
