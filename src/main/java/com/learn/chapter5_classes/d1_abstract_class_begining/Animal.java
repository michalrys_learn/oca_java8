package com.learn.chapter5_classes.d1_abstract_class_begining;

public abstract class Animal {
    public abstract void walk();

    abstract void printMe();
}
