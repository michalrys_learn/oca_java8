package com.learn.chapter5_classes.d1_abstract_class_begining;

public interface Moveable {
    void run();

    void walk();

    void fly();

}
