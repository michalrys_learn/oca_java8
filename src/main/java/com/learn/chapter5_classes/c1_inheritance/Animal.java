package com.learn.chapter5_classes.c1_inheritance;

public class Animal {
    private int age;

    public Animal(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }
}
