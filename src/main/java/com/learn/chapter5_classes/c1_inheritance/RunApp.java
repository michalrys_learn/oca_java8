package com.learn.chapter5_classes.c1_inheritance;

public class RunApp {
    public static void main(String[] args) {
        Animal dogA = new Dog(7, "insumnia");
        System.out.printf("My dog is %d years old.\n", dogA.getAge());
        if (dogA instanceof Dog) {
            ((Dog) dogA).bark();
        }
    }
}
