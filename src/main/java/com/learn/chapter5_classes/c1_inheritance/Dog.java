package com.learn.chapter5_classes.c1_inheritance;

public class Dog extends Animal{
    private Virus virus;
    private String other;

    public Dog(int age) {
        super(age);
    }

    public Dog(int age, String virusName) {
        super(age);
        this.virus = new Virus(virusName);
    }

    public Dog(int age, String virusName, String other) {
        this(age, virusName);
        this.other = other;
    }

    public void bark() {
        System.out.println("HAU HAU HAU");
        System.out.println("my virus is called: " + virus.getName());
    }

    private class Virus{
        private String name;

        public Virus(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
