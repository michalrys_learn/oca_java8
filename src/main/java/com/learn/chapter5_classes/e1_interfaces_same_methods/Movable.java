package com.learn.chapter5_classes.e1_interfaces_same_methods;

public interface Movable {
    void run();

    default void test() {
        System.out.println("test Movable");
    }
}
