package com.learn.chapter5_classes.e1_interfaces_same_methods;

public class Person implements Movable, Runable {

    @Override
    public void run() {
        System.out.println("run");
    }

    @Override
    public void test() {
        Runable.super.test();
    }
}
