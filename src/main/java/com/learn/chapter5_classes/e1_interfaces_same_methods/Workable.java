package com.learn.chapter5_classes.e1_interfaces_same_methods;

public interface Workable extends Movable, Runable {
    @Override
    default void test() {
        Movable.super.test();
    }

    <T> void print(T t);
}
