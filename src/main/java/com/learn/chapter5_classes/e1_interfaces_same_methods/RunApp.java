package com.learn.chapter5_classes.e1_interfaces_same_methods;

public class RunApp {
    public static void main(String[] args) {
        Person person = new Person();
        person.run();
        person.test();
    }
}
