package com.learn.chapter5_classes.c3_inheritance_method_overriding;

public class Animal {
     public boolean isOK() {
        return true;
    }

    public void getIsOk() {
        System.out.println(isOK());
    }
}
