package com.learn.chapter5_classes.c3_inheritance_method_overriding;

public class Cat extends Animal {
    public boolean isOK() {
        return false;
    }

    public void getIsOk() {
        System.out.println(isOK());
    }
}
