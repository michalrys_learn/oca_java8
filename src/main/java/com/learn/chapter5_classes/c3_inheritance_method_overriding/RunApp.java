package com.learn.chapter5_classes.c3_inheritance_method_overriding;

public class RunApp {
    public static void main(String[] args) {
        Animal animal = new Animal();
        animal.getIsOk();
        System.out.println(animal.isOK());

        Cat cat = new Cat();
        cat.getIsOk();
        System.out.println(cat.isOK());

        Animal catPoly = new Cat();
        catPoly.getIsOk();
        System.out.println(catPoly.isOK());
    }
}
