package com.learn.chapter4_encapsulation.staticinitializations.myClass;

public class MyClass {
    static {
        System.out.println("My super class was initatialized.");
    }
    {
        System.out.println("non static code block");
    }

    private static final String name;
    static {
        name = "test";
    }

    public MyClass() {
        System.out.println(name);
    }

    public static void printMe(String me) {
        System.out.println(me);
    }
}
