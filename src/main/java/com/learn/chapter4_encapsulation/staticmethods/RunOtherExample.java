package com.learn.chapter4_encapsulation.staticmethods;

import static com.learn.chapter4_encapsulation.staticmethods.OtherExample.*;

public class RunOtherExample {
    public static void main(String[] args) {
        OtherExample.test();
        OtherExample otherExample = new OtherExample();
        System.out.println(L);
    }
}
