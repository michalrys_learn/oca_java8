package com.learn.chapter4_encapsulation.question10;

import com.learn.chapter4_encapsulation.question10.rope.*;
import static com.learn.chapter4_encapsulation.question10.rope.Rope.*;

public class Chimp {
    public static void main(String[] args) {
        Rope.swing();
        new Rope().swing();
        System.out.println(LENGTH);
    }
}
