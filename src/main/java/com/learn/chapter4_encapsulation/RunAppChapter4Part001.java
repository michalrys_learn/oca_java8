package com.learn.chapter4_encapsulation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RunAppChapter4Part001 {
    public static void main(String[] args) {
        get(2);
        get(2, 2, 3, 4);
        get(2, new int[]{1, 2, 4});
        get(2, null);

        List<String> my = asList(new String[]{"ala", "ola"});
        List<String> other = Arrays.asList(new String[]{"ala", "ola"});

        System.out.println(my);
        System.out.println(other);
    }

    final synchronized private static void get(int a, int... elems) {
        if (elems == null) {
            System.out.println("VARARGS is null !");
            return;
        }
        System.out.printf("Passed %d and %d other elements.\n", a, elems.length);
    }

    public static <T> List<T> asList(T... a) {
        System.out.println("my list");
        ArrayList<T> values = new ArrayList<>();
        values.add(a[0]);
        return values;
    }
}
