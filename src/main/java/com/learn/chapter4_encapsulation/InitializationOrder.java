package com.learn.chapter4_encapsulation;

public class InitializationOrder {
    private String name = "Torchie";                   // #5

    {
        System.out.println(name);                        // #6
    }

    private static int COUNT = 0;     // #1

    static {
        System.out.println(COUNT);       // #2
    }

    {
        COUNT++;                                                  // #7
        System.out.println(COUNT);                                  // #8
    }

    public InitializationOrder() {
        System.out.println("constructor");                             // #9
    }

    public static void main(String[] args) {
        System.out.println("read to construct");  //#3
        new InitializationOrder();                  // #4
    }
}