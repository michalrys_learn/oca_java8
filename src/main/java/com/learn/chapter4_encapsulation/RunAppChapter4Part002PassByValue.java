package com.learn.chapter4_encapsulation;

public class RunAppChapter4Part002PassByValue {
    public static void main(String[] args) {

        String name = "Ala";
        changeName(name);
        System.out.println(name);

        StringBuilder surname = new StringBuilder("Pies");
        changeSurname(surname);
        System.out.println(surname);

        StringBuilder age = new StringBuilder("22");
        changeAge(age);
        System.out.println(age);
    }

    private static void changeName(String name) {
        name = "Ola";
    }

    private static void changeSurname(StringBuilder surname) {
        surname.delete(0, surname.length());
        surname.append("Kot");
    }

    private static void changeAge(StringBuilder age) {
        age = new StringBuilder("33");
    }


}
