package com.learn.chapter4_encapsulation.overloadingmethods;

public class RunApp {
    public static void main(String[] args) {
        add(2.5);
        System.out.println(method());

        RunApp runApp = new RunApp();
        runApp = null;
        runApp.method();

        int[] values = {1, 2, 3};
        System.out.println(values.length);
//        Array;
//        Object
        System.out.println(values.hashCode());
        System.out.println(values.toString());
        System.out.println(values.equals(new int[]{1, 2, 3}));
        System.out.println(values.getClass());
        System.out.println(values.getClass().getSimpleName());
        System.out.println(values.getClass().getName());
        System.out.println(values.getClass().getConstructors());
        System.out.println(2__3);
    }

    static void add(String a) {
    }

    //static void add(int a) {    }
    static void add(Integer a) {
    }

    static void add(Integer... a) {
    }

    static void add(Object a) {
    }
    //static void add(int [] a) {    }

    static float method() {
        return 2;
    }
}
