package com.learn.chapter4_encapsulation.lambda1;

public class Animal {
    private String spacies;
    private boolean canHop;
    private boolean canSwim;

    public Animal(String spacies, boolean canHop, boolean canSwim) {
        this.spacies = spacies;
        this.canHop = canHop;
        this.canSwim = canSwim;
    }

    public boolean canHop() {
        return canHop;
    }

    public boolean canSwim() {
        return canSwim;
    }

    @Override
    public String toString() {
        return spacies;
    }
}
