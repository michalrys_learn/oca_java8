package com.learn.chapter4_encapsulation.lambda1;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class RunApp {
    public static void main(String[] args) {
        List<Animal> animals = new ArrayList<>();
        animals.add(new Animal("fish", false, true));
        animals.add(new Animal("kangaroo", true, false));
        animals.add(new Animal("rabbit", true, false));
        animals.add(new Animal("turtle", false, true));

        print(animals, new CheckTrait() {
            @Override
            public boolean test(Animal animal) {
                return animal.canSwim();
            }
        });

        print(animals, a -> a.canSwim());
        print(animals, animal -> animal.canSwim());
        print2(animals, animal -> animal.canSwim());

        Predicate<Animal> canSwim = a -> a.canSwim();
        boolean test = canSwim.test(animals.get(0));
        System.out.println(test);

        animals.removeIf(a -> a.canSwim());

    }

    private static void print(List<Animal> animals, CheckTrait checkTrait) {
        for (Animal animal : animals) {
            if (checkTrait.test(animal)) {
                System.out.print(animal + " ");
            }
        }
        System.out.println();
    }
    private static void print2(List<Animal> animals, Predicate<Animal> predicate) {
        for (Animal animal : animals) {
            if (predicate.test(animal)) {
                System.out.print(animal + " ");
            }
        }
        System.out.println();
    }



}
