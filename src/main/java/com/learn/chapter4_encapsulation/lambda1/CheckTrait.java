package com.learn.chapter4_encapsulation.lambda1;

public interface CheckTrait {
    boolean test(Animal animal);
}
