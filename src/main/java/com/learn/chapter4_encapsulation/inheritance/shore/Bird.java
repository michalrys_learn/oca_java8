package com.learn.chapter4_encapsulation.inheritance.shore;

public class Bird {
    protected String text = "floating";

    protected void floatInWater() {
        System.out.println(text);
    }
}
