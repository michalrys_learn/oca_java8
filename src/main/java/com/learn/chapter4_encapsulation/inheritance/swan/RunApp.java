package com.learn.chapter4_encapsulation.inheritance.swan;

public class RunApp {
    public static void main(String[] args) {
        Swan swan = new Swan();
        swan.swim();
        swan.helpOtherSwam();

    }
}
