package com.learn.chapter4_encapsulation.inheritance.swan;

import com.learn.chapter4_encapsulation.inheritance.shore.Bird;

public class Swan extends Bird {
    public void swim() {
        floatInWater();
        System.out.println(text);
    }

    public void helpOtherSwam() {
        Swan other = new Swan();
        other.floatInWater();
        System.out.println(other.text);
    }
}
