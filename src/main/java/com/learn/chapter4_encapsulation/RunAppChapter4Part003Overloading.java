package com.learn.chapter4_encapsulation;

public class RunAppChapter4Part003Overloading {
    public static void main(String[] args) {
        getName();
        getName("Ola");

        throw new RuntimeException();

    }

    private static void getName() {
        System.out.println("Ala");
    }

    public static final String getName(String name) {
        System.out.println(name);
        return name;
    }

    public void convert(int a) {
        System.out.println(a);
    }

    public void convert(Integer a) {
        System.out.println(a);
    }
}
