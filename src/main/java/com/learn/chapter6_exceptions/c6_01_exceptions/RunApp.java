package com.learn.chapter6_exceptions.c6_01_exceptions;

public class RunApp {
    public static void main(String[] args) {
        try {
            throw new WrongCharException();
        } catch (RuntimeException e) {
            System.out.println("Exception was caught.");
//        } catch (WrongCharException e) {

        }
    }
}

class WrongNameException extends RuntimeException {
}

class WrongLetterException extends WrongNameException {
}

class WrongCharException extends WrongLetterException {
}

