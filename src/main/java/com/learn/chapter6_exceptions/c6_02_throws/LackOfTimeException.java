package com.learn.chapter6_exceptions.c6_02_throws;

public class LackOfTimeException extends Exception {
    public LackOfTimeException(String message) {
        super("no time: " + message);
    }
}
