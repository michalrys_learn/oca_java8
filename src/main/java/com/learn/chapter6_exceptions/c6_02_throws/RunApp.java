package com.learn.chapter6_exceptions.c6_02_throws;

public class RunApp {
    public static void main(String[] args) {
        try {
            print();
        } catch (NullPointerException e) {
            System.out.println("caught");
        }

        try {
            print2();
        } catch (RuntimeException e) {
            System.out.println("caught 2");
        }

        try {
            noTime();
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
        }
    }

    private static void print() throws NullPointerException {
        System.out.println("do nothing");
        throw new NullPointerException("o o o");
    }

    private static void print2() {
        System.out.println("do nothing 2");
        throw new NullPointerException("o o o");
    }

    private static void noTime() throws LackOfTimeException {
        System.out.println("no time for me");
        throw new LackOfTimeException("o o o");
    }
}