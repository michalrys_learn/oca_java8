package com.learn.chapter6_exceptions.c6_03_catch;

public class RunApp {
    public static void main(String[] args) {
        System.out.println("A");
        try {
            printMe();
        } catch (IllegalAccessError error) {
            System.out.println("not that error");
        } catch (NullPointerException e) {
            System.out.println("Null pointer was occured");
        } finally {
            System.out.println("C");
        }
        System.out.println("D");
    }

    private static void printMe() {
        System.out.println("B");
        throw new NullPointerException();
    }
}
