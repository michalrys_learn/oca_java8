package com.learn.chapter6_exceptions.c6_03_catch;

public class RunApp4CatchExceptionTwice {

    public static void main(String[] args) {
        System.out.println("1");
        try {
            System.out.println("2");
            throw new RuntimeException("Ala ma kota");
        } catch (RuntimeException e) {
            System.out.println("3");
            throw new RuntimeException("Ola ma psa");
        } finally {
            System.out.println("4");
            throw new RuntimeException("Hubert ma żółwia");
        }
    }
}

