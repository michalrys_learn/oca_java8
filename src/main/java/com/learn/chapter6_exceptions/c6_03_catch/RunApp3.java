package com.learn.chapter6_exceptions.c6_03_catch;

import java.io.IOException;

public class RunApp3 {
    public static void main(String[] args) throws IOException {
        printMe();
        throw new IOException();
    }

    private static Exception myException() {
        return new Exception();
    }

    private static void printMe() throws NullPointerException {
        System.out.println("Could throw nullpointerexception");

    }
}
