package com.learn.chapter6_exceptions.c6_03_catch;

public class RunApp2 {
    public static void main(String[] args) {
        try {
            System.out.println("1");
            throw new NullPointerException();
        } catch (NullPointerException e) {
            System.out.println("2");
            throw e;
        }

        //System.out.println("3");

    }
}
