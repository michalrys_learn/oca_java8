package com.learn.chapter2_operators_statement;

public class Chapter2Learn2RunApp01 {
    public static void main(String... args) {
        System.out.println("Short circuit operators 1");
        int c = 0;
        if (true || ++c == 1) {
            System.out.printf("c = %d\n", c);
        }

        System.out.println("Short circuit operators 2");
        int d = 0;
        if (false && false || ++c == 2 || ++d == 1) {
            System.out.printf("c = %d, d = %d\n", c,  d);
        }

        System.out.println("Short circuit operators 3");
        if (++d == 2 || ++c == 3 && true) {
            System.out.printf("c = %d, d = %d\n", c,  d);
        }

        int x;
        if ((x = 3 - c) > 0) {
            System.out.println("yes");
        }
    }
}
