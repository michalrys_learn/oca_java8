package com.learn.chapter2_operators_statement.L01;

public class RunApp8again {
    public static void main(String[] args) {

//        char a = 'b';
//        int b = a + 1;
//
//        System.out.println((int) a);
//        System.out.println(b);
//        System.out.println(4);

        byte x = 2;
        int y = 128;
        x *= y;
        System.out.println((byte) 128);
        System.out.println(x);
//        y *= x;
//        System.out.println(y);
        System.out.println(3 | 2);

        Person person = new Person();

        if (Sex.MALE != person.getSex()) {
            System.out.println("not a male or empty");
            System.out.println(person.getSex());
        } else {
            System.out.println("male");
        }

        System.out.println(3 == 'a');

        int a = 3;
        if (a > 4) ;
        System.out.println(a);
        System.out.println("===");
        int t = 20;
        while (t > 0) {
            do {
                t -= 2;
            } while (t > 5);
            t--;
            System.out.print(t + "\t");
        }

        System.out.println();
        System.out.println("------");
//        int i = 1;
//        i = i = i + 1;
//        i = i++;
//        System.out.println(i);
//        System.out.println("---");
//

//        for (int i = 0; i < 10; ) {
//            i = i++;
//            System.out.println(i + "Hello World");
//        }

//        System.out.println(1 / 0);
        System.out.println(1 / 0.0);
    }


}
