package com.learn.chapter2_operators_statement.L01;

public class RunApp5LogicalOperators {
    public static void main(String[] args) {

        int a = 1;
        int b = -4;
        // 3 = 0b011
        // 4 = 0b100
        // 7 = 0b111
        System.out.println("a = " + a + ", b = " + b);
        System.out.println("0b111 = " + 0b111);
        System.out.println("4 & 4 = " + (4 & 4));
        System.out.println("a & b = " + (a & b));
        System.out.println("a | b = " + (a | b));
        System.out.println("a ^ b = " + (a ^ b));
        System.out.println(3^4);
        System.out.println(Math.pow(2,4));

    }
}
