package com.learn.chapter2_operators_statement.L01;

public class RunAppSwitch {
    public static void main(String[] args) {
        char a = 'b';

        switch (a) {
            case 'a':
                System.out.println("here");
                System.out.println("a");
                int b = 2;
                System.out.println(b);
                break;
            case 'b':
                System.out.println("here");
                System.out.println("b");
                int a2 = 3;
                System.out.println(a2);
                break;
            default:
                break;
        }

        System.out.println("==========");


        switch ('a') {
            default:
                System.out.println("co kto ma");
            case 'a':
                System.out.println("ala ma kota");
                break;
            case 'b':
                System.out.println("ala ma psa");
                break;
        }

    }
}
