package com.learn.chapter2_operators_statement.L01;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RunApp6Labels {
    public static void main(String[] args) {

        List<Integer> values = new ArrayList<>();
        Random random = new Random();

        NEW_VALUE:
        for (int j = 1; j <= 5; j++) {
            LABLE_FOR_SECOND_FOR:
            for (int i = 1; i <= 10; i++) {
                if (values.size() == 8) {
                    break;
                }
                int value = random.nextInt(20);
                LABEL_FOR_IF:
                if (values.contains(value))
                    TEST2:{
                        System.out.println(value + " is in " + values);
                        continue NEW_VALUE;
                    }
                TEST3:
                {

                }
                TEST4: {

                }
                values.add(value);

            }
        }
        System.out.println(values);

    }
}
