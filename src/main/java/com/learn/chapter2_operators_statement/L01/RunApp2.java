package com.learn.chapter2_operators_statement.L01;

public class RunApp2 {
    public static void main(String[] args) {
        int i = 2;        boolean res = false;
        res = i++ == 2 || --i == 2 && --i == 2;
        System.out.println(res);
        System.out.println(i);

        i = 2;
        System.out.println(i);
        System.out.println(i++ == 3);
    }


}
