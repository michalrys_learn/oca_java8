package com.learn.chapter2_operators_statement.L01;

public class Person {
    private Sex sex;

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Sex getSex() {
        return sex;
    }
}
