package com.learn.chapter2_operators_statement.L01;

public class RunApp1 {
    public static void main(String[] args) {

        int value = -4;
        System.out.println(value >> 1);
        System.out.println(-0b1000);
        System.out.println(-0b1000 >> 1);

        System.out.println("ala" instanceof String);

        System.out.println(1 | 0);

        System.out.println("--");
        System.out.println(1/3);
        System.out.println(2/3);
        System.out.println(3/3);
        System.out.println("--");
        System.out.println(1%3);
        System.out.println(2%3);
        System.out.println(3%3);
        System.out.println("===");
        System.out.println(4.3 % 0.3);

        int a = 3;
        long b = 4;
        long l = a * b;

        float c = 2.1F;

        short aa = 3;
        short bb = 1;
        int i = aa / bb;
        short i1 = (short)(aa + bb);

        aa += bb;
        short i2 = aa;




    }
}
