package com.learn.chapter2_operators_statement.L01;

public class RunApp3 {
    public static void main(String[] args) {
        int i = 1;

        // byte = 8bits
        //   0 -> 0b00000000
        //   1 -> 0b00000001
        // 127 -> 0b01111111
        //  -1 -> 0b11111111
        //  -2 -> 0b11111110
        //-128 -> 0b10000000
        byte b = (byte) (127 + 128 + 1 + 127); //
        byte r = (byte) 0b11111110;
        System.out.println("b=" + b);
        System.out.println("r=" + r);
        System.out.println((byte) 1234567890123456789L);

    }
}
