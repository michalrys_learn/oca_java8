package com.learn.chapter2_operators_statement.L01;

public class RunApp4CompoundAssignmentOperator {
    public static void main(String[] args) {

        int a = 2;
        double b = 2.3;

        a *= b;
        System.out.println(a);

        double c = 0.3;
        int d = 2;
        c *= d;
        System.out.println(c);

        byte e = 4;
        int f = 400;
        e *= f;
        f *= e;
        System.out.println(e);
        System.out.println(f);

    }
}
