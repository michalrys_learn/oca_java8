package com.learn.chapter2_operators_statement.L01;

class AntelopeM {
    AntelopeM(int p) {
        System.out.print("4_constrA ");
    }

    {
        System.out.print("2_blockA ");
    }

    static {
        System.out.print("1_staticA ");
    }
}

class GazelleMod extends AntelopeM {
    {
        System.out.print("block1G ");
    }

    public GazelleMod(int p) {
        super(6);
        System.out.print("3_constrG ");
    }

    {
        System.out.print("block2G ");
    }

    static {
        System.out.print("static1G ");
    }

    public static void main(String[] hopping) {
        System.out.print("psvm_pre-newG ");
        new GazelleMod(0);
        System.out.println("psvm_post-newG ");
    }

    {
        System.out.print("9_blockG ");
    }

    static {
        System.out.print("8_staticG ");
    }

}