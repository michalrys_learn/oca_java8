package com.learn.chapter2_operators_statement.L01;

import java.util.Scanner;

public class RunApp7Labels {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        WHILE_LEVEL:
        while (true) {
            System.out.println("Give me name:");
            String value = scanner.nextLine();

            READ_NAME: //this does not work
            {
                SWITCH_LEVEL:
                switch (value) {
                    case "ola":
                        OLA_CASE:
                        {
                            System.out.println("Ola was chosen - CASE 1");
                            System.out.println("interesting");
                            break OLA_CASE;
                        }
                        OLA_CASE_2:
                        {
                            System.out.println("Ola was chosen - CASE 2");
                            System.out.println("interesting");
                            break SWITCH_LEVEL;
                        }
                    case "ala":
                        System.out.println("Ala was chosen");
                        System.out.println("great");
                        break WHILE_LEVEL;
                    default:
                        System.out.println("ups, sth else");
                        System.out.println("try again");
                        break READ_NAME; //this does not work
                }
                OLA_CASE_3:
                {
                    System.out.println("Ola was chosen - OUTSIDE SWITCH");
                    continue WHILE_LEVEL;
                }
            }
            System.out.println("asdf");

        }

    }
}
