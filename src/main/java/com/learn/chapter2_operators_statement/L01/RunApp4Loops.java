package com.learn.chapter2_operators_statement.L01;

import com.learn.chapter1_building_blocks.chapter1day2.Person;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class RunApp4Loops {
    public static void main(String[] args) {
        int i = 0;
        do System.out.println(++i);
        while (i <= 10);

        System.out.println("--");

        int b = 3;
        LABEL_DO:
        do while (b < 10) System.out.println(b++);
        while (b < 10);

        System.out.println("--");

        for (Person person = new Person("Ala"); !person.getName().equals("Tom"); person.setName(getRandomName())) {
            System.out.println(person.getName());
        }

        for (; LocalTime.now().isBefore(LocalTime.parse("22:07", DateTimeFormatter.ofPattern("HH:mm"))); ) {
            LABEL_TEST:
            System.out.println("s");
        }


    }

    private static String getRandomName() {
        List<String> names = Arrays.asList("Ala", "Ola", "Ula", "Tom", "Fred");
        Random random = new Random();
        int id = random.nextInt(names.size());
        return names.get(id);
    }

}
