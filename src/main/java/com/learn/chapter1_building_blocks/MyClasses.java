package com.learn.chapter1_building_blocks;

class MyClasses {
    public static void main(String[] args) {
        OtherClasses ala = new OtherClasses("ala");
        System.out.println(ala.getName());
    }
}

class OtherClasses {
    private String name;

    public OtherClasses(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
