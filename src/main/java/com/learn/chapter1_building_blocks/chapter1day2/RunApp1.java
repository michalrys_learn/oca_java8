package com.learn.chapter1_building_blocks.chapter1day2;

public class RunApp1 {
    public static void main(String[] args) {

        int results = (int) (3.5 + 3);
        System.out.println(results);

        System.out.println(3.5 + 3);
        float b = 23.1F;
        double c = 23.1;
        char sign = 'a';
        long d = 123L;

        System.out.println(b);

        // other number system
        System.out.println("Other systems");

        int octal = 016;
        System.out.println(octal);
        int hexadecimal = 0xff;
        System.out.println(hexadecimal);
        int binary = - 0b0011;
        System.out.println(binary);

        int maxValue = Integer.MAX_VALUE;
        System.out.println(Integer.toBinaryString(Integer.MAX_VALUE));
        System.out.println(Long.toBinaryString(Long.MAX_VALUE));
        long binaryMax = 0b111111111111111111111111111111111111111L;
        System.out.println(binaryMax);
        System.out.println(0xFFF);


        System.out.println(0b001_001);
        System.out.println(1_000_000.150_001);

        Person person = null;
        System.out.println(person);
        System.out.println(person != null ? person.getName() : "is null");

        double value = 0xF;
        double valueC = 0b101;
        double valueD = 1;
        System.out.println(valueD);

        int Public = 12;

        long x = 2_147_000_000;
        

    }
}
