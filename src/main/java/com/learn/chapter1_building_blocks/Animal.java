package com.learn.chapter1_building_blocks;

/**
 * This is Animal like cat or dog. It has {@name}.
 * @name String
 */
public class Animal {
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    /**
     * This method return name.
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * This method set name.
     * @param name String
     */
    public void setName(String name) {
        this.name = name;
    }
}
