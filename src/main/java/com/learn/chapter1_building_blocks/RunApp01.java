package com.learn.chapter1_building_blocks;

import java.util.Date;

public class RunApp01 {
    public static void main(String[] arg) {
        /*
        System.out.println(2_0_0_1);
        System.out.println(2_001);
        */
        System.out.println(2_001_1);


        Animal animal = new Animal("tiger");
        animal.setName("cat");
        System.out.println(animal.getName());

        Date dateA = new Date();
        java.sql.Date dateB = new java.sql.Date(12l);

    }
}
