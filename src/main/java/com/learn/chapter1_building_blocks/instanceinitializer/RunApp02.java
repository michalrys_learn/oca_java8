package com.learn.chapter1_building_blocks.instanceinitializer;

public class RunApp02 {
    public static void main(String[] args) {

        Dog azor = new Dog("Azor");
        System.out.println(azor.getName());
    }
}
