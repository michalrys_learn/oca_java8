package com.learn.chapter1_building_blocks.instanceinitializer;

public class Dog {

    static {
        System.out.println("Dog");
    }

    private String name;

    public Dog(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        System.out.println("hello");
    }

    public String getName() {
        return name;
    }

    {
        name = "some dog";
        System.out.println(this.name);
    }
}
