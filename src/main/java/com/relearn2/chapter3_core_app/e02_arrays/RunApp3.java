package com.relearn2.chapter3_core_app.e02_arrays;

import java.util.Arrays;

public class RunApp3 {
    public static void main(String[] args) {
        Object[] values = new Object[5];
        values[0] = "ala";
        values[1] = new Integer(2);
        values[2] = new StringBuilder("tes");

        System.out.println(Arrays.toString(values));
        System.out.println("===================");

        Object[] other = {"ala", "tola", "ola"};
        other[1] = new StringBuilder("test");
        other[2] = new Integer(34);
        System.out.println(Arrays.toString(other));

        System.out.println("----");
        String[] texts = {"ala", "ola", "tola"};
        Object[] textsObject = texts;
        String[] texts2 = (String[]) textsObject;
        //texts2[0] = new Integer(2);
//        textsObject = new Integer(2);
        System.out.println(Arrays.toString(texts2));

    }
}
