package com.relearn2.chapter3_core_app.e02_arrays;

import java.util.Arrays;

public class RunAppEmptyArrays {
    public static void main(String[] args) {
        int matrix[][] = new int[][]{{}, {2, 3, 4}, {2}};
        System.out.println(matrix.length);
        System.out.println(matrix[0].length);
        System.out.println(matrix[1].length);
        System.out.println(matrix[2].length);

        System.out.println(Arrays.toString(matrix[0]));
        System.out.println(Arrays.toString(matrix[1]));
        System.out.println(Arrays.toString(matrix[2]));

        System.out.println("-----");

        int[] empty = {};
        System.out.println(Arrays.toString(empty));
        System.out.println(empty.length);
        System.out.println(empty[0]);
    }


}
