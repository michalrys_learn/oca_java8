package com.relearn2.chapter3_core_app.e02_arrays;

import java.lang.reflect.Array;
import java.util.Arrays;

public class RunAppArraysA {
    public static void main(String[] args) {
        int[] a1 = new int[]{1, 2, 3};
        int[] a2 = new int[]{1, 5, 3};

        boolean equals = a1.equals(a2);
        System.out.println(equals);
        System.out.println(a1);
        System.out.println(a2);


        for (int i = 0; i < a1.length; i++) {
            String comparison = a1[i] == a2[i] ? "the same" : "other";
            System.out.printf("%d) are %s\n", i, comparison);
        }

        int[] a3 = new int[3];
        System.out.println(Arrays.toString(a2));
        System.out.println(Arrays.toString(a3));


        int[] a4 = {1, 2, 3};

        printMe(new int[]{1, 2, 3});

        int[] b1, b2;
        int[] c1, c2[], c3[], c4[][];

        c3 = new int[3][5];
        c3[1][3] = 34;
        for (int[] i : c3) {
            System.out.println(Arrays.toString(i));
        }

        int d1[], d2, d3[][];
        d1 = new int[]{1, 2, 3};
        d2 = 23;
        d3 = new int[][]{{}, {1, 2, 3}, {}};
        System.out.println(Arrays.toString(d3[1]));
        System.out.println(d3[0][0]);
    }

    private static void printMe(int... values) {
        System.out.println(Arrays.toString(values));
    }
}
