package com.relearn2.chapter3_core_app.e02_arrays;

import java.util.Arrays;

public class RunApp4 {
    public static void main(String[] args) {
        String[] values = {"1", "10", "20", "222", "9"};
        Arrays.sort(values);
        System.out.println(Arrays.toString(values));

        int[][][] a = new int[3][][];
        a[0] = new int[3][];
        a[0][1] = new int[3];
        a[0][0] = new int[]{1, 2, 3};
    }
}
