package com.relearn2.chapter3_core_app.e04_datetime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;

public class RunApp1 {
    public static void main(String[] args) {
        LocalTime timeA = LocalTime.of(23, 33);
        LocalDateTime dateTimeA = LocalDateTime.of(2023, 12, 21, 22, 15);

        LocalTime timeAplus = timeA.plusHours(2);
        System.out.println(dateTimeA);
        System.out.println(timeA);
        System.out.println(timeAplus);

        System.out.println(dateTimeA);
        Period period = Period.ofDays(23);
        LocalDateTime plus = dateTimeA.plus(period);
        System.out.println(plus);

        System.out.println("  Ala  ".trim().toLowerCase());

        System.out.println("Formatting");
        System.out.println(dateTimeA.format(DateTimeFormatter.ISO_LOCAL_DATE));
        System.out.println(dateTimeA.format(DateTimeFormatter.ISO_DATE_TIME));
        DateTimeFormatter shortStyle = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
        System.out.println(dateTimeA.format(shortStyle));
        System.out.println(dateTimeA.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)));
        System.out.println("--");
        System.out.println(dateTimeA.format(DateTimeFormatter.ofPattern("yyyy-MMMM-dd-HH-mm-ss")));
        System.out.println("--");

        LocalDate parsedValue = LocalDate.parse("2023-12-12", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        System.out.println(parsedValue);

//        LocalDate.of()

        DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yy-MMMM-d");
        LocalDate someDateOnly = LocalDate.of(2023, Month.FEBRUARY, 24);
        String someDateOnlyFormatted = dtf2.format(someDateOnly);
        System.out.println(someDateOnlyFormatted);

        System.out.println(("a" + "la") == "ala");
        String a = "a";
        a += "la";
        System.out.println(a == "ala");
        System.out.println((a + "la") == "ala");

        String b = "a" + "la";
        System.out.println(b == "ala");


        LocalDate date1 = LocalDate.of(2022, 02, 02);
        boolean after = date1.equals(LocalDate.of(2022, 2, 2));
        System.out.println(after);

        System.out.println(date1);
        LocalDate parse = LocalDate.parse("2023-02-02");
        System.out.println(parse);
        System.out.println(date1.getDayOfMonth());

        System.out.println(date1.withDayOfYear(50));

        Period period2 = Period.of(0, 1000, 0);
        System.out.println(period2);
        System.out.println(Period.of(1,0,3));
        System.out.println(Period.of(0,0,0));

        System.out.println(LocalTime.of(0,1,1).getHour());;
        System.out.println(LocalTime.of(23,1,1).getHour());

        System.out.println(LocalTime.MAX);
    }

}
