package com.relearn2.chapter3_core_app.e04_datetime;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class RunApp {
    private static int age;

    public static void main(String[] args) {

        //StringBuilder test = new StringBuilder("test").insert(test.length(), "s");
        //System.out.println(test);

        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter format = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
        System.out.println(format.format(now));

    }
}
