package com.relearn2.chapter3_core_app.e12_arrays_udemy1.question001;

import java.util.ArrayList;
import java.util.List;

public class RunAppPersons {
    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>();

        persons.add(new Person("Tom", 12));
        persons.add(new Person("Tom", 12));
        persons.add(new Person("Andreas", 15));
        persons.add(new Person("Tom", 12));
        persons.add(new Person("Tommy", 22));

        boolean remove = persons.remove(new Person("Tom", 12));
        System.out.printf("Person was removed ? : %s\n", remove);
        System.out.println(persons);
    }
}
