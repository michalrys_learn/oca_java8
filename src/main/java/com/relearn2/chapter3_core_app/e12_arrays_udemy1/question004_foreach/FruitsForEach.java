package com.relearn2.chapter3_core_app.e12_arrays_udemy1.question004_foreach;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FruitsForEach {
    public static void main(String[] args) {
        List<String> fruits = new ArrayList<>();
        fruits.add("Pineapple");
        fruits.add("Apple");
        fruits.add("Melon");
        fruits.add("Avocado");
        fruits.add("Watermelon");

        //removeFruitStartingWithAUsingForEachLoop(fruits);
        //removeFruitStartingWithAUsingWhileLoop(fruits);

        // THIS WORKS !!!
        Iterator<String> iterator = fruits.iterator();
        while (iterator.hasNext()) {
            String fruit = iterator.next();
            System.out.printf("%s -> %s\n", fruits, fruit);
            if(fruit.startsWith("A")) iterator.remove();
        }
        System.out.println(fruits);

    }

    private static void removeFruitStartingWithAUsingWhileLoop(List<String> fruits) {
        Iterator<String> iterator = fruits.iterator();
        while (iterator.hasNext()) {
            String fruit = iterator.next();
            System.out.printf("%s -> %s\n", fruits, fruit);
            if (fruit.startsWith("A")) fruits.remove(fruit);
        }
    }

    private static void removeFruitStartingWithAUsingForEachLoop(List<String> fruits) {
        for (String fruit : fruits) {
            System.out.printf("%s -> %s\n", fruits, fruit);
            if (fruit.startsWith("A")) {
                fruits.remove(fruit);
            }
        }
    }
}
