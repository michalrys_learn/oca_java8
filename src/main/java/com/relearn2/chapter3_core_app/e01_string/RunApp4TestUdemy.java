package com.relearn2.chapter3_core_app.e01_string;

public class RunApp4TestUdemy {
    public static void main(String[] args) {
        String a = "ala";
        String b = "ola";

        System.out.println(a = b);

        System.out.println("ala" == ("ala" + "" ));

        final String name = "ala";
        String name2 = name + " r";
        System.out.println("ala r" == name2);
        System.out.println(name == "ala");

        final int i1 = 1;
        final char c1 = 'a';
        final double d1 = 1.0;
        final float f1 = 1.0f;
        final Integer i2 = new Integer(1);

        System.out.println("1a" == i1 + "a");
        System.out.println("1a" == i2 + "a");
        System.out.println("aa" == c1 + "a");
        System.out.println(("1.0a" == d1 + "a") + "->" +  d1 + "a");
        System.out.println("1.0a" == f1 + "a");

        StringBuilder sb = new StringBuilder();
        sb.append("ala");
        sb.delete(1, 10000);
        System.out.println(sb.toString());

        String text = "ala";
     //   System.out.println(text.substring(0, 100));

        String txt = null;
        System.out.println(txt + null);
    }
}
