package com.relearn2.chapter3_core_app.e01_string;

public class RunApp1 {
    public static void main(String[] args) {
        String www = "http://www.onet.pl/?address=alamakota&userId=michal&id=123123";

        int idAddress = www.indexOf("address");
        int idNextAmpersand = www.indexOf("&", idAddress);
        String address = www.substring(idAddress + "address".length(), idNextAmpersand);
        System.out.printf("My address is %s.\n", address);

        StringBuilder results = new StringBuilder();
        StringBuilder ala = results.append("Ala");
        results.append(" ma kota");

        System.out.println(ala);

        String substring = ala.substring(ala.indexOf("ma") + "ma".length());
        System.out.println(substring.trim());

        results.delete(0, 5);
        System.out.println(results);

        System.out.println("-----------");
        StringBuilder content = new StringBuilder();
        content.append("Ala ma kota\n");
        content.append("Ola ma psa\n");
        content.append("A ja nie mam zwierzecia\n");
        content.deleteCharAt(content.length() - 1);
        System.out.println(content);
    }

}
