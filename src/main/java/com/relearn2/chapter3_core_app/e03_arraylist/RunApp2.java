package com.relearn2.chapter3_core_app.e03_arraylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RunApp2 {
    public static void main(String[] args) {
        List<Integer> values = Arrays.asList(4, -2, 5, 0);
        Collections.sort(values);
        Integer[] array = values.toArray(new Integer[4]);
        System.out.println(Arrays.toString(array));
    }
}
