package com.relearn2.chapter3_core_app.e03_arraylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

public class RunApp1 {
    public static void main(String[] args) {
        ArrayList a = new ArrayList();
        a.add("34");
        a.add("33");
        a.add(23);
        a.add(true);
        a.add(new Integer(34));
        a.add(new Integer(34));
        a.add(new Person("tom"));
        System.out.println(a);
        //
        List<String> b = new ArrayList<>();
        b.add("33");
        b.add(String.valueOf(true));

        System.out.println(a.indexOf("33"));
        Object removedBySet = a.set(1, "50");
        System.out.println(a);
        System.out.println(removedBySet);

        Integer integer = Integer.valueOf("23");
        int i = Integer.parseInt("23");

        List<Double> values = new ArrayList<>();
        values.add((double) 23);
        values.add(0, 22.);
        values.set(1, 22.);
        System.out.println(values);


        String[] array = b.toArray(new String[0]);
        Double[] array1 = values.toArray(new Double[0]);
        System.out.println(Arrays.toString(array1));

        Integer[] c = {1, 2, 3};
        List<Integer> list = Arrays.asList(c);
//        list.add(34);

        Collections.sort(list);


    }
}
