package com.relearn2.chapter3_core_app.e03_arraylist;

import java.util.ArrayList;
import java.util.List;

public class RunApp3 {
    public static void main(String[] args) {
        List a = new ArrayList<String>();
        List<String> b = new ArrayList<>();

        a.add("tes");
        a.add(2);
        System.out.println(a);
        System.out.println(a.get(1).getClass().getName());

        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        sb.append("ala");
        sb2.append("ala");
        boolean theSame = sb.equals(sb2);
        System.out.println(theSame);


    }
}
