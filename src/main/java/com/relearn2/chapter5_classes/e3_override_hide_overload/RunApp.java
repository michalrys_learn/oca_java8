package com.relearn2.chapter5_classes.e3_override_hide_overload;

public class RunApp {
    public static void main(String[] args) throws Exception {
        Person personStudent = new Student();
        personStudent.printMe("Notes");
        ((Student) personStudent).printMe("Notes", "2023-01-23");

        Student normalStudent = new Student();
        normalStudent.printMe("Exam");

        // static methods
        System.out.println(personStudent.getType());
        System.out.println(normalStudent.getType());

        //field hide
        System.out.println(personStudent.name);
        System.out.println(normalStudent.name);
        System.out.println(personStudent.getName());
        System.out.println(normalStudent.getName());

    }

}
