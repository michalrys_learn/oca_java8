package com.relearn2.chapter5_classes.e3_override_hide_overload;

public class Student extends Person{
    public String name = "StudentName";

    public void printMe(String txt) {
        super.printMe(txt + " - printed by student");
    }

    public void printMe(String txt, String note) throws Exception {
        super.printMe(txt + " - note: " + note);
    }

    public static String getType() {
        return "Student";
    }
}
