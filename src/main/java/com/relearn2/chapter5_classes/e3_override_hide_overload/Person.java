package com.relearn2.chapter5_classes.e3_override_hide_overload;

public class Person {
    public String name = "PersonName";

    public void printMe(String txt) {
        System.out.println(txt);
    }

    public static void printType() {
        System.out.println("Person");
    }

    public static String getType() {
        return "Person";
    }
    public String getName() {
        return name;
    }
}
