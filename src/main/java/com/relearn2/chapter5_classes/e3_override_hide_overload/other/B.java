package com.relearn2.chapter5_classes.e3_override_hide_overload.other;

public class B extends A{
    @Override
    public String getName() {
        String name = super.getName();
        return name + " R.";
    }
}
