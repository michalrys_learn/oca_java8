package com.relearn2.chapter5_classes.f2_poly;

public class C extends B{
    @Override
    void printMe() {
        System.out.println("print me in C");
    }

    public void printC() {
        System.out.println("print only in C");
    }
}
