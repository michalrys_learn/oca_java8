package com.relearn2.chapter5_classes.f2_poly;

public class B extends A{
    @Override
    void printMe() {
        System.out.println("print me in B");
    }

    public void printB() {
        System.out.println("print only in B");
    }
}
