package com.relearn2.chapter5_classes.f2_poly;

public class RunApp {
    public static void main(String[] args) {
        A ba = new B();
        ba.printMe();
        ((B) ba).printB();

        ((A) ba).printMe();
//        ((C) ba).printMe();

//        C bc = (C) ba;

//        C cb = (C) new B();

        System.out.println("------");
        A ca = new C();
        B cb = (B) ca;
        C cc = (C) ca;
        cb.printB();
        ca.printMe();

    }
}
