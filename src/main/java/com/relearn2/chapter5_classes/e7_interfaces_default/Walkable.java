package com.relearn2.chapter5_classes.e7_interfaces_default;

public interface Walkable {
    default void move() {
        System.out.println("I walk.");
    }
}
