package com.relearn2.chapter5_classes.e7_interfaces_default;

public interface Runnable {
    default void move() {
        System.out.println("I run");
    }
}
