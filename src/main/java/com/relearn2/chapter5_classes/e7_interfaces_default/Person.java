package com.relearn2.chapter5_classes.e7_interfaces_default;

public class Person implements Walkable, Runnable{
    @Override
    public void move() {
        Runnable.super.move();
    }
}
