package com.relearn2.chapter5_classes.e8_polymorphism;

public class RunApp1 {
    public static void main(String[] args) {
        Person person = new Person();
        Object personObject = person;
        Human personHuman = person;

        person.getVoice();
        System.out.println(personHuman.getType());
        ((Person) personObject).printMe();

//        Integer personInteger = person;
//        Student personStudent = person;

        System.out.println("--- student ---");
        Student student = new Student();
        Human studentHuman = student;
        studentHuman.getVoice();
        student.getType();

        ((Person) student).getVoice();
        ((Human) student).getVoice();

        Student personStudent = (Student) person; // ! be careful
        personStudent.getVoice();
    }
}
