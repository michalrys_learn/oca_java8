package com.relearn2.chapter5_classes.e8_polymorphism;

public interface Human {
    String getType();

    void getVoice();
}
