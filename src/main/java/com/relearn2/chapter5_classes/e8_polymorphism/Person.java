package com.relearn2.chapter5_classes.e8_polymorphism;

public class Person implements Human{
    @Override
    public String getType() {
        return "Person";
    }

    @Override
    public void getVoice() {
        System.out.println("I am a person");
    }

    public void printMe() {
        System.out.println("Person");
    }
}
