package com.relearn2.chapter5_classes.e8_polymorphism;

public class Student extends Person{
    @Override
    public String getType() {
        return "student";
    }

    @Override
    public void printMe() {
        System.out.println("Print me -> student");
    }

    @Override
    public void getVoice() {
        System.out.println("I am a student");
    }
}
