package com.relearn2.chapter5_classes.e4_hide_static_met;

public class Student extends Person {
    public static String getType() {
        return "Student";
    }

    @Override
    public String get() {
        return getType();
    }
}
