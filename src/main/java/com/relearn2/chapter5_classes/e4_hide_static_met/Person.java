package com.relearn2.chapter5_classes.e4_hide_static_met;

public class Person {
    public static String getType() {
        return "Person";
    }

    public String get() {
        return getType();
    }
}
