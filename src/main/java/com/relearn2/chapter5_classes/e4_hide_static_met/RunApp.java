package com.relearn2.chapter5_classes.e4_hide_static_met;

public class RunApp {
    public static void main(String[] args) {
        Person studentPoly = new Student();

        System.out.println(studentPoly.get());
        System.out.println(studentPoly.getType());
    }
}
