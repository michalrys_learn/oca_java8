package com.relearn2.chapter5_classes.e2_order;

public class NormalStudent extends Student{
    {
        System.out.println("student normal - code block");
    }

    public NormalStudent() {
        System.out.println("student normal - constructor");
    }

    static {
        System.out.println("student normal - static code block");
    }
}
