package com.relearn2.chapter5_classes.e2_order;

public class RunApp {
    public static void main(String[] args) {
        Student studentNull;
        System.out.println("---");
        Student studentPoly = new NormalStudent();
        System.out.println("---");
        NormalStudent normalStudent = new NormalStudent();
    }
}
