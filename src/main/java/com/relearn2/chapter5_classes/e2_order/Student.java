package com.relearn2.chapter5_classes.e2_order;

public class Student extends Person{
    {
        System.out.println("student - code block");
    }
    public Student() {
        System.out.println("student - constructor");
    }
    static {
        System.out.println("student - static code block");
    }
}
