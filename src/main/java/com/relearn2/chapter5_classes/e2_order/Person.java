package com.relearn2.chapter5_classes.e2_order;

public class Person {
    {
        System.out.println("person - code block");
    }
    private String name = "field value";
    public Person() {
        System.out.println("person - constructor 1st line");
        System.out.println("person - constructor 2nd line: " + name);
    }
    static {
        System.out.println("person - static code block");
    }
}
