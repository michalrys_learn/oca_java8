package com.relearn2.chapter5_classes.e6_abstract_overriden;

public class Kowalski extends Person{
    void printMe() {
        System.out.println("Kowalski");
    }
    public void getName() {
        System.out.println("I am a Kowalski");
    }
}
