package com.relearn2.chapter5_classes.e6_abstract_overriden;

public abstract class Person extends Student {
    abstract void printMe();
    abstract public void getName();
}
