package com.relearn2.chapter5_classes.e1_start;

public class RunApp {
    public Test test = new Test();

    public static void main(String[] args) {
        RunApp runApp = new RunApp();
        runApp.test.print();

        System.out.println(runApp.test.get());
    }

    public Test get() {
        return this.test.get();
    }

    private class Test {
        void print() {
            System.out.println("private class - test print");
        }

        public Test get() {
            return this;
        }

        public String getTxt() {
            return "private class";
        }
    }
}
