package com.relearn2.chapter5_classes.f1_start;

public interface Runnable {
    int SIZE = 3;

    void printMe();

    static int add(int a, int b) {
        return a + b;
    }

    default void print() {
        System.out.println("print");
    }
}
