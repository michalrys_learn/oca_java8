package com.relearn2.chapter5_classes.f1_start;

public abstract class Student extends Person{
    public static void printMe() {
        System.out.println("I am a Student");
    }

    public void printMeAgain() {
        super.doSth();
    }

    abstract void print();
}
