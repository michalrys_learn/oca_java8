package com.relearn2.chapter5_classes.f1_start;

public class Bird implements Runnable, Swimmable{
    @Override
    public void print() {
        Runnable.super.print();
    }

    @Override
    public void printMe() {
        System.out.println("print me");
    }
}
