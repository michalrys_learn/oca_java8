package com.relearn2.chapter5_classes.f1_start;

public interface Swimmable {
    int SIZE = 5;

    void printMe();

    static int add(int a, int b) {
        return a + b;
    }

    default void print() {
        System.out.println("print");
    }

}
