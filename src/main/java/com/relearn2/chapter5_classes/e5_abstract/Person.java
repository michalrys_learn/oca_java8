package com.relearn2.chapter5_classes.e5_abstract;

public class Person implements Human, Animal{

    public void print() {
        Human.printMe();
    }

    @Override
    public void getVoice() {
        System.out.println("Roar");
    }

    @Override
    public void getVoice(String name) {
        System.out.println(name);
    }
}
