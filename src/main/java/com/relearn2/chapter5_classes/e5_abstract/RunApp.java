package com.relearn2.chapter5_classes.e5_abstract;

public class RunApp {
    public static void main(String[] args) {
        Worker worker = crateWorker("name", "surname");

    }

    private static Worker crateWorker(String name, String surname) {
        return new Worker() {
            @Override
            public void getName() {
                System.out.println(name);
            }

            @Override
            protected void getSurname() {
                System.out.println(surname);
            }
        };
    }
}
