package com.relearn2.chapter5_classes.e5_abstract;

public interface Human {
    void getVoice(String name);

    static void printMe() {
        System.out.println("Human interface");
    }
}
