package com.relearn2.chapter5_classes.e5_abstract;

public class RunAppManyInterfaces {
    public static void main(String[] args) {
        Person person = new Person();
        Human personHuman = new Person();

        person.print();
        person.getVoice();
    }
}
