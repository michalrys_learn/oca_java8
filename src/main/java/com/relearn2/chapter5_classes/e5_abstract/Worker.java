package com.relearn2.chapter5_classes.e5_abstract;

public abstract class Worker {
    public abstract void getName();
    protected abstract void getSurname();
}
