package com.relearn2.chapter5_classes.e5_abstract;

public interface Animal {
    void getVoice();

    static void printMe() {
        System.out.println("Animal interface");
    }
}
