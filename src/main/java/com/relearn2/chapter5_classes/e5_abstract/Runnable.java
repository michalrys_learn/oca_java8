package com.relearn2.chapter5_classes.e5_abstract;

import javax.sql.rowset.CachedRowSet;
import java.util.concurrent.Callable;

public abstract interface Runnable extends Callable, CachedRowSet {
    void walk();

    static void print() {
        System.out.println("print static method");
    }

    default void print(String txt) {
        System.out.println(txt);
    }
}
