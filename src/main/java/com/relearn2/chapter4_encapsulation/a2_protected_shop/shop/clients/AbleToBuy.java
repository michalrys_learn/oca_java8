package com.relearn2.chapter4_encapsulation.a2_protected_shop.shop.clients;

public interface AbleToBuy {
    boolean buy(String productName);
}
