package com.relearn2.chapter4_encapsulation.a2_protected_shop.shop;

import com.relearn2.chapter4_encapsulation.a2_protected_shop.shop.clients.NormalClient;

public class RunApp {
    public static void main(String[] args) {
        Client clientA = new NormalClient("Ala");

        System.out.println(clientA);
    }
}
