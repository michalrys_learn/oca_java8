package com.relearn2.chapter4_encapsulation.a2_protected_shop.shop;

public class Client {
    protected String name;
    protected int age;
    protected String type;

    public Client(String type) {
        this.type = type;
    }

    protected String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", type='" + type + '\'' +
                '}';
    }
}
