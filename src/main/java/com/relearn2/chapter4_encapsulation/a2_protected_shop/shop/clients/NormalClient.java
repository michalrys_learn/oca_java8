package com.relearn2.chapter4_encapsulation.a2_protected_shop.shop.clients;

import com.relearn2.chapter4_encapsulation.a2_protected_shop.shop.Client;

public class NormalClient extends Client implements AbleToBuy {
    public NormalClient(String name) {
        super("Normal Client");
        this.name = name;
    }

    public NormalClient(int age) {
        super("Normal Client");
        this.age = age;
    }

    @Override
    public boolean buy(String productName) {
        return false;
    }
}
