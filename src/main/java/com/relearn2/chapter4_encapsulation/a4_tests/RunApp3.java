package com.relearn2.chapter4_encapsulation.a4_tests;

public class RunApp3 {
    final static int age = 1;
    private static int age2;

    static {
        age2 = 2;
    }

    public String name;

    public static void main(String[] args) {
        System.out.println("test");
    }

}
