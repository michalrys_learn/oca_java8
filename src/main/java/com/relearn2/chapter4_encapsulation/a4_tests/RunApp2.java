package com.relearn2.chapter4_encapsulation.a4_tests;

public class RunApp2 {
    public static void main(String[] args) {
        StringBuilder text = new StringBuilder();
        update(text);
        update2(text);
        System.out.println(text.toString());
        System.out.println("---------------------");
        printMe(3);
        printMe('a');
    }

    private static void update(StringBuilder text) {
        text = new StringBuilder();
        text.append("ala ma kota");
    }

    private static void update2(StringBuilder text) {
        text.append("Ola ma psa");
    }

    private static void printMe(Integer value) {
        System.out.println("Intiger was used");
    }

    private static void printMe(int value) {
        System.out.println("int was used");
    }
}
