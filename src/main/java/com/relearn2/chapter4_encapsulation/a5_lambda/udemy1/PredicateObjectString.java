package com.relearn2.chapter4_encapsulation.a5_lambda.udemy1;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class PredicateObjectString {
    public static void main(String[] args) {
        List<String> values = new ArrayList<>();
        values.add("ala");
        values.add("alicja");
        values.add("ola");

        //Predicate pr1 = name -> name.length() <= 3;
        Predicate<String> pr2 = name -> name.length() <= 3;

        values.removeIf(pr2);
        System.out.println(values);
    }
}
