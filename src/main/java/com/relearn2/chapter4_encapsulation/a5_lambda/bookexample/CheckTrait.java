package com.relearn2.chapter4_encapsulation.a5_lambda.bookexample;

public interface CheckTrait {
    boolean test(Animal a);
}
