package com.relearn2.chapter4_encapsulation.a5_lambda.bookexample;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class TraditionalSearch {
    public static void main(String[] args) {
        List<Animal> animals = new ArrayList<>();
        animals.add(new Animal("fish", false, true));
        animals.add(new Animal("kangaroo", true, false));
        animals.add(new Animal("rabbit", true, false));
        animals.add(new Animal("turtle", false, true));

        print(animals, new CheckIfHopper());

        print(animals, animal -> animal.canSwim());
        print(animals, (Animal animal) -> animal.canSwim());
        print(animals, animal -> {
            return animal.canSwim();
        });
        print(animals, (a) -> a.canHop());
        print(animals, Animal::canSwim);

        CheckTrait canHop = a -> a.canHop();
        CheckTrait canSwim = Animal::canSwim;
        canHop.test(animals.get(0));


        printPredicate(animals, an -> !an.canSwim());
        animals.removeIf(a -> !a.canSwim());
        System.out.println(animals);

    }

    private static void print(List<Animal> animals, CheckTrait checker) {
        for (Animal animal : animals) {
            if (checker.test(animal)) {
                System.out.print(animal + " ");
            }
        }
        System.out.println();
    }

    private static void printPredicate(List<Animal> animals, Predicate<Animal> predicate) {
        for (Animal animal : animals) {
            if (predicate.test(animal)) {
                System.out.print(animal + " ");
            }
        }
        System.out.println();
    }
}
