package com.relearn2.chapter4_encapsulation.a5_lambda.bookexample;

public class CheckIfHopper implements CheckTrait {
    @Override
    public boolean test(Animal a) {
        return a.canHop();
    }
}
