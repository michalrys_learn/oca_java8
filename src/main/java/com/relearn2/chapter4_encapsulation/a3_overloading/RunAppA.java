package com.relearn2.chapter4_encapsulation.a3_overloading;

public class RunAppA {
    public static void main(String[] args) {

    }

    private static void print(String... values) {
        for (String i : values) {
            System.out.println(i);
        }
    }


//    public static String print(String[] args) { // zonk}

}
