package com.relearn2.chapter4_encapsulation.a3_overloading;

public class RunAppB {
    public static void main(String[] args) {
        print(2);
        Student student = new Student();
        print(student);

        Worker worker = new Worker();
        print(worker);
    }

    public static void print(Number value) {
        System.out.println("print Integer");
    }

    public static void print(Person value) {
        System.out.println("print Person");
    }

    public static void print(Student value) {
        System.out.println("print Student");
    }
}
