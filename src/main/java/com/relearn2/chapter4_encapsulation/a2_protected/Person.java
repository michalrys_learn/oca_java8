package com.relearn2.chapter4_encapsulation.a2_protected;

public class Person {
    protected String name;

    protected void printProtected() {
        System.out.println("Protected method, name = " + name);
    }
}
