package com.relearn2.chapter4_encapsulation.a2_protected;

import com.relearn2.chapter4_encapsulation.a2_protected.other.Student;

public class RunAppHigher {
    public static void main(String[] args) {
        Person person = new Person();
        person.name = "Tom";
        person.printProtected();

        Student student = new Student();
        student.name = "Mike";
        student.printProtected();

        Person studentPoly = new Student();
        studentPoly.name = "Poly";
        studentPoly.printProtected();
    }
}
