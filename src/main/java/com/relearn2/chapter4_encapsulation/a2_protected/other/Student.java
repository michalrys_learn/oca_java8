package com.relearn2.chapter4_encapsulation.a2_protected.other;

import com.relearn2.chapter4_encapsulation.a2_protected.Person;

public class Student extends Person {
    public static void main(String[] args) {
        Student student = new Student();
        student.name = "Mike";
        student.printProtected();
        student.privatePrint();

        Person studentPoly = new Student();
        ((Student) studentPoly).name = "Mike";
        ((Student) studentPoly).printProtected();

        Person person = new Person();

        RunAppLower runAppLower = new RunAppLower();

    }

    private void privatePrint() {
        System.out.println("Private print");
    }
}
