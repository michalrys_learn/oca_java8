package com.relearn2.chapter4_encapsulation.a2_protected.other;

import com.relearn2.chapter4_encapsulation.a2_protected.Person;

public class RunAppLower extends Student {
    public static void main(String[] args) {
        Person person = new Person();
        Student student = new Student();

        RunAppLower runAppLower = new RunAppLower();
        runAppLower.name = "elo";
        runAppLower.printProtected();
    }
}
