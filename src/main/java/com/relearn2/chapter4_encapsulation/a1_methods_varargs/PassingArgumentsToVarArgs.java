package com.relearn2.chapter4_encapsulation.a1_methods_varargs;

public class PassingArgumentsToVarArgs {
    public static void main(String[] args) {
        printMe();
        printMe(new String[]{"test", "me"});
        printMe("test", "me");
        printMe(null);

        System.out.println("-----------------");

        printNumbers(null);
    }

    public static void printMe(String... args) {
        if (args == null) {
            System.out.println("null :(");
            return;
        }
        for (String arg : args) {
            System.out.println(arg);
        }
    }

    private static void printNumbers(int... values) {
        if (values == null) {
            System.out.println("Oh no! there are no numbers");
            return;
        }
        for (int i : values) {
            System.out.println(i);
        }
    }
}
