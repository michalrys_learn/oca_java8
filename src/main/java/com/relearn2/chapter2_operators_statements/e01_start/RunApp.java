package com.relearn2.chapter2_operators_statements.e01_start;

public class RunApp {
    public static void main(String[] args) {
        int a1 = 0b01;
        int a3 = 01234567;
        int a2 = 0xF1;

        byte b = 122 + 5;
        System.out.println(b);

        int b1 = 0b001;
        int b2 = 0b010;
        System.out.println(Integer.toBinaryString(b1 | b2));

        String test = "ad";
        System.out.println(test == "ad");


    }
}
