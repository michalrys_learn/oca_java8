package com.relearn2.chapter2_operators_statements.e01_start;

public class RunApp3 {
    public static void main(String[] args) {

        for (int i = 0, j = 0; i <= 5 && j <= 5; i++, j++) {
            System.out.println(i + ", " + j);
        }

        int b = 0;
        for (int a = 0; a <= 10; a++) {
            b = 2 * ++b;
            System.out.println(b);
        }

        int i = 1;
        i = i++ + i--;
        System.out.println(i);

        System.out.println(2/5);

        String name = "ala";
        final String b22 = "ola";
        switch (name){
            case b22:
                System.out.println("yes");
        }

        System.out.println(2^1);
        int x1 = 2;
        int x2 = 1;
        System.out.println(x1 ^ x2);
    }
}
