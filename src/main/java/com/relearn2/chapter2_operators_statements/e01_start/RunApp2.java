package com.relearn2.chapter2_operators_statements.e01_start;

import com.relearn2.chapter5_classes.f2_poly.B;

public class RunApp2 {
    public static void main(String[] args) {
        int a = 2;

        Object isGreater = a > 0 ? 3 : false;
        System.out.println(isGreater);

        String name = "Ala2";

        switch (name) {
            case "Ala": {
                System.out.println(name);
                System.out.println("and");
            }
            default:
                System.out.println("other");
                System.out.println("break");
            case "Ula":
                System.out.println("Ula");
                break;
        }

        a = 50;
        do if (a >= 0) System.out.print("-");
        else System.out.println(""); while (a-- >= 0);

        a = 50;
        do {
            if (a >= 0) {
                System.out.print("-");
            } else {
                System.out.println("");
            }
        } while (a-- >= 0);

        int x = 0;
        for (int a2 = 0; a2 >= 0 && x <= 10; x++, a++) {
            System.out.println(x);
            x += a2 + a;
        }

        for (x = 10, a = 3; x <= 20; x++) {
            System.out.println(x);
            x += a;
        }

        System.out.println("=====");
        for (int z = 0; z <= 10; ) {
            System.out.println(z);
            z++;
        }


    }
}