package com.relearn2.chapter1_building_blocks.e02_arrays;

import java.util.Arrays;

public class RunApp1 {
    public static void main(String[] args) {
        int[][] a = new int[3][1];
        int[] b[];
        b = new int[][]{{1, 1}, {2, 2}, {5}};
        for (int[] i : b) {
            System.out.println(Arrays.toString(i));
        }
    }
}
