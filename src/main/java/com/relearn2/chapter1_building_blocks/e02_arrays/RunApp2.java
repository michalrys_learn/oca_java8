package com.relearn2.chapter1_building_blocks.e02_arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RunApp2 {
    public static void main(String[] args) {
        List<Integer> values = new ArrayList<>();
        values.add(50);
        values.add(6);
        values.add(2, 10);
        System.out.println(values);

        List<Integer> valuesB = new ArrayList<>();
        valuesB.add(10);
        valuesB.add(5);

        boolean remove = values.remove(Integer.valueOf("6"));

        System.out.println(values);
        Integer[] array = values.toArray(new Integer[0]);
        System.out.println(Arrays.toString(array));

        Collections.sort(values);
        System.out.println(values);
        System.out.println(Arrays.toString(array));

        values.add(34);
        System.out.println(values);
        System.out.println(Arrays.toString(array));

    }
}
