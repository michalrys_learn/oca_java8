package com.relearn2.chapter1_building_blocks.e02_arrays;

import java.util.Arrays;
import java.util.List;

public class RunApp3 {
    public static void main(String[] args) {
        Integer[] array = {300, 5, 6, 20};
        List<Integer> list = Arrays.asList(array);

        array[1] = 100;
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
        System.out.println(list);

    }
}
