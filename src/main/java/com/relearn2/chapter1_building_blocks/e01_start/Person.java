package com.relearn2.chapter1_building_blocks.e01_start;

class Car {
    public static void main(String[] args) {
        System.out.println("Some car class");
    }

    public void printMe() {
        System.out.println("This is car");
    }


    @Override
    protected void finalize() throws Throwable {
        System.out.println("It was deleted");
    }
}

class Bicycle {
    public static void main(String[] args) {
        System.out.println("Some bicycle");
    }

    public String get() {
        return "Bicycle";
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("It was deleted");
        Car car = new Car();
        car.printMe();
        System.gc();
    }
}
