package com.relearn2.chapter1_building_blocks.e01_start;

import java.util.*;
import java.sql.*;
import java.util.Date;

public class RunApp2 {
    public static void main(String[] args) throws InterruptedException {
        Car car = new Car();
        Car.main(new String[]{"Elo elo"});
        car.printMe();

        Bicycle bicycle = new Bicycle();
        System.out.println(bicycle.get());

        Human human = new Human();
        Date date = new Date();

        tempUse();
        System.gc();

        for (int i = 1; i <= 50; i++) {
            System.out.println(i);
            Thread.sleep(500);
        }
        System.out.println("end");

    }

    public static void tempUse() {
        Car car = new Car();
        car.printMe();
    }
}
