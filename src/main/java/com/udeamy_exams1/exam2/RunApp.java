package com.udeamy_exams1.exam2;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.BiConsumer;

class A1 {
    public static void main(String[] args) {
        if (2 > 4) {
            System.out.println("yes 1");
        }
        if (2 > 5) System.out.println("yes 2");
        else System.out.println("no 2");

        StringBuilder b1 = new StringBuilder();
        StringBuilder b2 = new StringBuilder();

        b1.append("ala");
        b2.append("ala");

        System.out.println(b1.equals(b2));

        Object a = "test";
        System.out.println(a);

        ArrayList arrayList = new ArrayList();

        LocalDate now = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        System.out.println(formatter.format(now));
    }
}

class C2 {
    public static void main(String[] args) {
        StringBuilder b = new StringBuilder("ala");
        System.out.println(b.equals("ala"));

        String s = "oca";
        StringBuilder sb = new StringBuilder("oca");

        System.out.println(s.equals(sb) + " " + sb.equals(s));
    }
}


class A2 {
    public static void main(String[] args) {
        int x = 2;
        boolean res = false;
        res = x++ == 2 || --x == 2 && --x == 2;
        System.out.println(x);
    }
}

class A3 {
    public static void main(String[] args) {
        Double[] doubleArray = new Double[2];
        System.out.println(doubleArray[0] + doubleArray[1]);

        StringBuilder builder = new StringBuilder();
    }
}

class A4 {
    public static void main(String[] args) {
        int a = 2;
        switch (a) {
            case (2 + 0):
                System.out.println("iok");
                System.out.println("asdf");
            case 3:
                //sdf
            default:
                break;
        }
    }
}

class A5 {
    static String b;

    public static void main(String[] args) {
        String t = "a";
        t += 'a';
        System.out.println(t == "aa");

        System.out.println((int) 'X');

        String b;
        b = "ala";
        System.out.println(b);
    }
}


class A6 {
    public static void main(String[] args) {

    }
}


interface Moveable {
    int move(int distance);
}

class Person {
    static int DEFAULT_DISTANCE = 5;
    int age;
    float height;
    boolean result;
    String name;
}

class MainClass {
    public static void main(String arguments[]) {
        Person p = new Person();
        Moveable moveable = (x) -> Person.DEFAULT_DISTANCE + x;
        System.out.println(p.name + p.height + p.result + p.age + moveable.move(10));
    }
}

class A7 {
    public static void main(String[] args) {
        System.out.println("ala" + 'r');
        System.out.println("ala" + true);
        System.out.println("ala" + null);
        System.out.println("ala" + Double.NaN);


    }
}





