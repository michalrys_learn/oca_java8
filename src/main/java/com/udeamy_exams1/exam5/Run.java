package com.udeamy_exams1.exam5;

import com.udeamy_exams1.exam5.other.Message22;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

class A {
    public static void main(String[] args) {
        long a = 100, b = 100;
        System.out.println(String.valueOf(a + b).length());

        {
            System.out.println("as");
        }

        Period period = Period.of(5, 2, 0).ofYears(10);
        System.out.println(period);

        LocalDate now = LocalDate.now();
        System.out.println(now);
        System.out.println(now.plusYears(1).plusDays(10));

        System.out.println(" ".isEmpty());
        System.out.println(" ".trim().isEmpty());
        System.out.println("ala ma kota    ".trim());
        System.out.println("ala ma kota  \n  ".trim());

        StringBuilder builder = new StringBuilder();

        byte a1 = 1;
        byte a2 = 2;
        double r = a1 + a2;

    }
}

class A2 {
    public static void main(String[] args) {
        StringBuilder builder = new StringBuilder();
        builder.append("ala ma kot");
        builder.delete(3, 100);
        System.out.println(builder);

        List<String> values = new ArrayList<>();
        values.add("ala");
        values.add("ola");
        values.add("ala");
        values.addAll(1, values);
        System.out.println(values);
    }
}

interface I1 {
    int age = 10;

    default void print() {
        System.out.println("i 1");
    }
}

interface I2 {
    int age = 20;

    default void print() {
        System.out.println("i 1");
    }
}

class A3 implements I1, I2 {
    public static void main(String[] args) {
        System.out.println(I1.age);
        System.out.println(I2.age);

        new A3().print();
    }

    public void print() {
        I1.super.print();
    }
}

class A4 {
    public static void main(String[] args) {
        try {
            throw new IOException();
        } catch (IOException e) {
            throw new RuntimeException();
        } finally {

        }
    }
}

class A5 {
    public static void main(String[] args) {
        short x = 0532;
        System.out.println(x);

        List<String> val = new ArrayList<>();
        val.add("aa");
        val.add("bb");
        val.add("aa");
        val.add("cc");

        Iterator<String> iterator = val.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            if (next.startsWith("a")) {
                iterator.remove();
            }
        }

        System.out.println(val);
    }
}

class A6 {
    public void print(int a, String b) {
        System.out.println("a");
    }

    public void print(String b, int a) {
        System.out.println("a");
    }

    public static void main(String[] args) {
        A6 a6 = new A6();
        a6.print(1, "s");
        a6.print("s", 1);
    }

}


class A7 {
    public void print() throws FileNotFoundException {

    }

    public static void main(String[] args) {
        A7 a7 = new A7();
        try {
            a7.print();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}

class B1 {
    public static void main(String[] args) {
        LocalDate date = LocalDate.parse("2022-04-30");
        System.out.println(date);
        double x = 10;
        System.out.println(x);
    }
}

class C1 {
    public static void main(String[] args) {
        boolean flag1 = true;
        final boolean flag2 = false;
        if (flag2) {
            System.out.print(3); //Line n3
        }
        System.out.println("asdf");

        Period parse = Period.parse("p-100y");
        System.out.println(parse);

        int a[] = {};
        final int i = 100;
        byte b = i;
    }
}

class D1 {
    public static void main(String[] args) {
        try {
            System.out.println("helo");

        } catch (Throwable e) {

        }

    }
}

class D2 {
    public static void main(String[] args) {
        Integer i = 10;
        List<Integer> list = new ArrayList<>();
        list.add(i);
        list.add(new Integer(10));
        list.add(i);

//        list.removeIf(y -> y == new Integer(10));
        list.removeIf(y -> y == 10);
        System.out.println(list);

        Integer b = 10;
        System.out.println(b == new Integer(10));
        System.out.println(b == new Integer("10"));
        System.out.println(b == Integer.valueOf("10"));
        System.out.println(b == Integer.parseInt("10"));

    }
}

class D3 {
    public static void main(String[] args) {
        int x = 5 * 4 % 3;
        System.out.println(x);
    }
}


abstract class Bird {
    private void fly() {
        System.out.println("Bird is flying");
    }

    public static void main(String[] args) {
        Bird bird = new Owl();
        bird.fly();
    }
}

class Owl extends Bird {
    protected void fly() {
        System.out.println("Owl is flying");
    }
}


class D5 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(10);
        list.add(new Integer(10));

        list.removeIf(i -> i == 10);
        System.out.println(list);

        System.out.println(new Integer(10) == 10);
        System.out.println(10 == new Integer(10));
        System.out.println(10 == Integer.valueOf(10));
        System.out.println(10 == Integer.valueOf("10"));
        System.out.println(10 == Integer.parseInt("10"));
        System.out.println("--");
        System.out.println(new Integer(10) == new Integer(10));
        System.out.println(new Integer(10) == Integer.valueOf("10"));
        System.out.println(new Integer(10) == Integer.parseInt("10"));

        System.out.println("==");
        Integer b = new Integer(20);
        Integer c = 20;
        Integer d = 20;
        System.out.println("TRUE");
        System.out.println(b == Integer.parseInt("20"));
        System.out.println(b == 20);
        System.out.println(c == Integer.valueOf(20));
        System.out.println(c == Integer.parseInt("20"));
        System.out.println(c == 20);
        System.out.println(c == d);

        System.out.println("FALSE");
        System.out.println(b == Integer.valueOf("20"));
        System.out.println(b == Integer.valueOf(20));
        System.out.println(b == new Integer(20));
    }
}

interface D7 {
    static void print() {
        System.out.println("PRINT");
    }
}

class D8 implements D7 {
    public static void print() {
        System.out.println("D8");
        String D7 = "sdfg";

    }
}


class D9 {
    public static void main(String args[]) {
        int arr[] = new int[5];
        arr = new int[]{1, 2, 3, 4};
        int result = arr[1]-- + arr[0]-- / arr[0] * arr[4];
        System.out.println(result);
    }
}


class A10 {
    final String print = "A";
}

class B10 extends A10 {
    /*INSERT CODE HERE*/
    String print = "A";
//    Object print = "A";
}

class MainClass {
    public static void main(String[] args) {
        B10 b = new B10();
        b.print = "Main";
        System.out.println(b.print);
    }
}

class MainClass2 {
    public static void main(String[] args) {
        String s = "oca";
        StringBuilder sb = new StringBuilder("oca");

        System.out.println(s.equals(sb) + " " + sb.equals(s));
    }
}


class MainClass9 {
    public static void main(String args[]) {
        Integer i = 1000;
        List<Integer> arr = new ArrayList<>();
        arr.add(i);
        arr.add(new Integer(i));
        arr.add(i);

        arr.removeIf(z -> z == 1000);
        System.out.println(arr);
    }
}

class D6 {
    public static void main(String[] args) {
        Integer a = new Integer(1000);
        Integer a2 = new Integer(1000);
        int b = 1000;
        int b2 = 1000;
        Integer c1 = new Integer(100);
        Integer c2 = new Integer(100);

        System.out.println(b == a);
        System.out.println(a2 == a);
        System.out.println(c1 == c2);
        System.out.println(b == b2);

        System.out.println(new Integer(100) == 100);
        System.out.println(new Integer(1000) == 1000);
        System.out.println(new Integer(1000) == new Integer(1000));
        System.out.println(new Integer(100) == new Integer(100));
    }
}

class E1 {
    public static void main(String[] args) {

        Object obj = new Integer(5);
        String str = (String) obj;
        System.out.println(str);

    }

}

class Car {
    static void drive() {
        System.out.println("relax");
    }
}

class SportCar extends Car {
    static void drive() {
        System.out.println("enjoy");
    }
}

class MainClass222 {
    public static void main(String... args) {
        Car car = new Car();
        Car sportCar = new SportCar();
        car.drive();
        sportCar.drive();

        System.out.println(-0);
    }
}

class AB1 {
    public static void main(String[] args) {
        LocalDate date = LocalDate.parse("2022-04-30");
        System.out.println(date.plusMonths(-2));
    }
}


class XMLMessage extends Message22 {
    String getText() {
        return "text";
    }

    public static void main(String[] args) {
        System.out.println(new XMLMessage().getText());
    }
}

















