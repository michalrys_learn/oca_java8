package com.udeamy_exams1.exam1;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class A {
    public static void main(String[] args) {
        int[][] a = new int[2][4];
        a[0] = new int[]{1, 2, 3, 4};
        a[1] = new int[]{1, 2};
        for (int[] outer : a) {
            for (int inner : outer) {
                System.out.print(inner);
            }
            System.out.println("");
        }
    }
}

class B {
    public static void main(String[] args) {

        String names[] = {"Thomas", "Peter", "Joseph"};
        String pwd[] = new String[3];
        int idx = 0;

        try {
            for (String n : names) {
                pwd[idx] = n.substring(2, 6);
                idx++;
            }

        } catch (Exception e) {
            System.out.println("Invalid Name");
        }

        for (String p : pwd) {
            System.out.println(p);
        }
    }
}

class C {
}

abstract class Vehicle {
    public int speed() {
        return 0;
    }
}

class Car extends Vehicle {
    public int speed() {
        return 60;
    }
}

class D {
    public static void main(String[] args) {
        StringBuilder builder = new StringBuilder();
        System.out.println(new StringBuilder("ala").equals(new StringBuilder("ala")));

        LocalDate dateA = LocalDate.of(2000, 01, 01);
        LocalDate dateB = LocalDate.of(2000, 01, 01);
        System.out.println(dateB.equals(dateA));
    }
}

class E {
    public static void main(String[] args) {
        System.out.println(Period.parse("-P100Y0M0D"));
        System.out.println(Period.parse("P-100Y0M0D"));
        System.out.println(Period.parse("p-100Y0M0D"));

        final boolean flag2 = false;
        final boolean flag3;
        flag3 = true;

        if (flag2) {
            System.out.print(4); //Line n4
        }
        System.out.print(5); //Line n5

        while (flag3) {
            System.out.println("asd");
        }
        System.out.println("sd");

    }
}

class F {
    public static void main(String[] args) {
        Object o = null;
        E e = (E) o;

        char a = 12;

        List<String> values = new ArrayList<>();
        values.add(null);
        values.add(null);
        values.add(null);
        System.out.println(values.remove(null));
    }
}

enum Sex {
    MALE, FEMALE;
}

class G {
    static String name;

    public static void main(String[] args) {
        System.out.println(name);
        Sex male = Sex.MALE;
        System.out.println(male.ordinal());
        throw null;
    }
}

class A0 {
}

class A1 extends A0 {
}

class A2 extends A1 {
}

class A3 extends A2 {
    public static void main(String[] args) {
        A3 a3 = new A3();
        System.out.println(a3 instanceof A0);
        System.out.println(a3 instanceof Object);

        A0 a0 = new A0();
        System.out.println(a0 instanceof A3);
    }
}

class B1 {
    public static void main(String[] args) {
        Boolean[] values = new Boolean[1];
        System.out.println(2 > 3 ? Boolean.TRUE : values[0]);
        System.out.println(2 > 3 ? true : values[0]);
    }
}

class B2 {
    static String a = "ala";
    String b = "ala";

    public static void main(String[] args) {
        Error error = new Error();
        System.out.println(error instanceof Object);
        System.out.println(error instanceof Throwable);
        System.out.println(error instanceof Error);

        String a = "adf";
        System.out.println(a);
        System.out.println(B2.a);

        String intern = a.intern();

        switch (a = "d") {
            default:
                System.out.println(a);
        }
    }
}

class B3 {
    public static void main(String[] args) {
        Integer a = 1;
        Integer b = 1;
        System.out.println(a == b);

        LocalDate of = LocalDate.of(1011, 07, 1);
        System.out.println(of);

        System.out.println('a' + 'b');
        System.out.println("ala " + 2 * 3);
    }
}

class Message {
    String msg = "X"; //Line n1

    Message() {
        Message(); //Line n2
    }

    void Message() { //Line n3
        System.out.println(msg += "A"); //Line n4
    }
}


class B5 {
    public static void main(String[] args) {
        List<Number> list = new ArrayList<>(
                Arrays.asList(10, new Integer(10), 10));
        if (list.get(0) == list.get(1))
            list.remove(10);
        if (list.get(0) == list.get(2))
            list.add(10);

        System.out.println(list);

        System.out.println(10 == new Integer(10));
        System.out.println(list.get(0) == list.get(1));
        System.out.println(list.get(0).getClass());
    }
}

class B6 {
    public static void main(String[] args) {
        List<LocalDate> dates = new ArrayList<>();
        dates.add(LocalDate.of(2025, Month.MARCH, 31).withMonth(4));
        System.out.println(dates);

        B6 b6 = new B6();
        b6.print();
    }

    private void print() {
        Human[] people = {new Human(), new Human()};
        Person[] persons = {new Person(), new Person()};
        people = persons;


        try {
            System.out.println(people);
        } catch (Throwable e) {

        }

    }

    private class Person extends Human {
    }

    private class Human {
    }
}

class C1 {
    public static void main(String[] args) {
        final String[] values = {"ala", "tola"};
        values[0] = "sdf";

//        values = new String[]{"asdf"};

        String a = "ala";
        switch (a) {
//            case values[0]:

        }
    }
}

class C2 {
    public static void main(String[] args) {
        byte a = 10;
        print(a);
//        print(10);

    }

    static void print(byte b1) {
        System.out.println(b1);
    }
}

class Z {
}

abstract class Y extends Z {
}

class Z1 {
    public static void main(String[] args) {
        Y y = (Y) new Z();
    }
}

class C3 {
    public static void main(String[] args) {
        List<Integer> values = new ArrayList<>();
        values.add(1);
        values.add(new Integer(2));
        values.add(Integer.parseInt("80000"));

        System.out.println(values.contains(80_000));
        System.out.println(values.contains(1));
        System.out.println(values.contains(2));

        values.remove(new Integer(80_000));
        System.out.println(values);

        System.out.println(100 == Integer.valueOf("100"));
        System.out.println(1_000 == Integer.valueOf("1000"));
        System.out.println(1_000 == new Integer(1000));
        System.out.println(1_000 == Integer.parseInt("1000"));

        int a = new Integer(1000);
        System.out.println((int) 1000 == (int) a);
        System.out.println("test");
        System.out.println(new Integer(1000) == new Integer(1000));
        System.out.println(new Integer(100) == new Integer(100));
//        System.out.println(1000 == new Integer(1000));
//        System.out.println(100 == new Integer(100));
//        System.out.println(new Integer(100).equals(100));
//
//        Integer a1 = 100;
//        Integer a2 = 100;
//
//        Integer b1 = 1000;
//        Integer b2 = 1000;
//
//        System.out.println(a1 == a2);
//        System.out.println(b1 == b2);
//
//        System.out.println(new Integer(1000) == new Integer(1000));
//        System.out.println(new Integer(100) == new Integer(100));

    }
}

class C4 {
    public static void main(String[] args) {
        Period p = Period.ofDays(1).ofYears(2);
        System.out.println(p);
        int[][] a = new int[3][];

        int length = a.length;
        int b = 2;

        if (2 > 4) ;
        else System.out.println("2");


//        int[][] bb = new int[][];
    }
}

class C5 {
    public static void main(String[] args) {
        int i = 10;
        do
            while (i < 15)
                i = i + 20;
        while (i < 20);
        System.out.println(i);
    }
}

class D1 {
    public static void main(String[] args) {
        byte[] a = {5, 5};
        a[0] = 23;
        a[1] = 23;

        System.out.println(Arrays.toString(a));

        print(new byte[]{1, 2});

        StringBuilder sb = new StringBuilder("ala ma kota");
        System.out.println(sb.deleteCharAt(sb.length() - 1));
        int aa = 0xFfF;
        int aaa = 0B101;
    }

    private static void print(byte[] a) {
        System.out.println(Arrays.toString(a));
    }
}

class C6 {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        sb.append("ala ma kota");
        sb.subSequence(1, 2);

        List<String> a = new ArrayList<>();
        a.add("ads");

        byte b1 = 1;
        int a1 = 100;
        a1 = b1;

        String aaa = //sdf
                "adf";
    }
}

class C7 {
    public static void main(String[] args) {
        System.out.println("OcA" == "OcA".replace('c', 'c'));
        System.out.println("OcA" == "OcA".replace("c", "c"));

        System.out.println(new Integer(0) == new Integer(0));

        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);
        System.out.println(formatter.format(LocalDateTime.now()));
    }
}





