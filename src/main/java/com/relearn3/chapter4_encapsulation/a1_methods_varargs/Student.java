package com.relearn3.chapter4_encapsulation.a1_methods_varargs;

import java.util.Arrays;

public class Student extends Person {
    @Override
    public void printMe(String a, String[] args) {
        System.out.println(Arrays.toString(args));
    }
}
