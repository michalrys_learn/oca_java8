package com.relearn3.chapter4_encapsulation.a1_methods_varargs;

public class RunAppPerson {
    public static void main(String[] args) {
        Person person = new Person();
        Student student = new Student();

        person.printMe("1", "ala", "ola");
        person.printMe("1", new String[]{"ala", "ola"});
//        student.printMe("1", "ala", "ola"); // this cause compilation error due to wrong method signature
        student.printMe("1", new String[]{"ala", "ola"});

    }
}
