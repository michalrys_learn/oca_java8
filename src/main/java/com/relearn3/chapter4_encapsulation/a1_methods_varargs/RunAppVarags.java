package com.relearn3.chapter4_encapsulation.a1_methods_varargs;

import java.util.Arrays;

public class RunAppVarags {
    public static void main(String[] args) {
        printMe(4);
        printMe(4, 1);
        printMe(4, 1, 2, 3);
        printMe(4, new int[]{1, 2, 3});
        printMe(4, null);

        printMe2(5, 2, 3, 4);
        printMe2(5);
        printMe2(5, null);
        printMe2(5, new int[]{1, 2, 3});
    }

    private static void printMe(int first, int... other) {
        System.out.printf("First value is %d.Others: ", first);
        if (other != null) {
            if (other.length == 0) {
                System.out.printf("---\n");
                return;
            }
            for (int i = 0; i < other.length - 1; i++) {
                System.out.printf("%d → ", other[i]);
            }
            System.out.printf("%d\n", other[other.length - 1]);
        } else {
            System.out.printf("null :(\n");
        }
    }

    private static void printMe2(int first, int... other) {
        String print = other != null ?
                other.length != 0 ?
                        other.length == 0 ?
                                String.format("Single element = %d\n", other[0])
                                : String.format("Several elements = %s\n", Arrays.toString(other))
                        : String.format("No elements\n")
                : String.format("Null :-(\n");
        System.out.printf("First value is %d. Others: %s", first, print);
    }
}
