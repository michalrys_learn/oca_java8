package com.relearn3.chapter4_encapsulation.a2_protected.a;

public class A {
    protected String name;
    public static String kind = "HUMAN";

    public A() {
        name = "Stefan";
    }

    protected String getName() {
        return name;
    }

    protected void printStatus() {
        System.out.println("status");
    }

    public static void print() {
        System.out.println("print static method");
    }
}
