package com.relearn3.chapter4_encapsulation.a2_protected.b;

import static com.relearn3.chapter4_encapsulation.a2_protected.a.A.kind;

public class C extends B {
    public static void main(String[] args) {
        B b = new B();
        b.printStatus();

        C c = new C();
        System.out.println(c.getName());

        System.out.println(kind);
        c.print();
    }
}
