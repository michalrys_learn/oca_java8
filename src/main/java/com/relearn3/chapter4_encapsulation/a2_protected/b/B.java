package com.relearn3.chapter4_encapsulation.a2_protected.b;

import com.relearn3.chapter4_encapsulation.a2_protected.a.A;

public class B extends A {
    @Override
    protected void printStatus() {
        System.out.println("Status modified.");
    }
}
