package com.relearn3.chapter4_encapsulation.a2_protected.b;

public class RunAppB {
    public static void main(String[] args) {
        B b = new B();
        b.printStatus();

        B.print();
        b.print();
    }
}
