package com.relearn3.chapter1_basics.a1;

import java.io.File;

public class RunApp {
    public static void main(String[] args) {
        int valueA = 1;
        double valueB = 2.0;
        boolean valueC = false;
        boolean valueD = true;
        System.out.println(valueA == valueB == valueC == valueD);
    }

    private static void testingA() {
        int a = 1;
        System.out.println(a == 1.0);
//        System.out.println(1 != null);
        File fileA = new File("c:\\test"), fileB = fileA;
        File fileC, fileD = new File("c:\\test"), fileE = fileA;

        System.out.println(fileA == fileB);
//        System.out.println(fileC == fileD);
        System.out.println(fileE == fileA == true);
    }

    private boolean isOne(int value) {
        return value == 1.0 ? true : false;
    }



}
