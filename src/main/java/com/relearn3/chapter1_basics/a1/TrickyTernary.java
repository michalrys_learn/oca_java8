package com.relearn3.chapter1_basics.a1;

public class TrickyTernary {
    public static void main(String[] args) {
        TrickyTernary trickyTernary = new TrickyTernary();
        Person who = 2 > 3 ? trickyTernary.getPerson() : trickyTernary.getStudent();
        System.out.println(who);

    }

    Person getPerson() {
        return new Person();
    }

    Student getStudent() {
        return new Student();
    }

    Worker getWorker() {
        return new Worker();
    }

    class Person {
        @Override
        public String toString() {
            return "person";
        }
    }

    class Student extends Person {
        @Override
        public String toString() {
            return "student";
        }
    }

    private Person trickyTernary() {
        return 2 > 3 ? new Person(): new Student();

    }
    class Worker {
        @Override
        public String toString() {
            return "worker";
        }

    }
}
