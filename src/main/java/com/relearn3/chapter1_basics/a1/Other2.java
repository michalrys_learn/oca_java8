package com.relearn3.chapter1_basics.a1;

public class Other2 {
    public static void main(String[] args) {
        int c = 1;
        System.out.println(2 > 3 ? ++c : c);

        String name = "ala";

        switch (name) {
            case "ala":
                System.out.println("ok");
            default:
                System.out.println("Default");
            case "a":
                System.out.println("not a");
                System.out.println("not b");
                break;
        }

        do {
            System.out.println("do operation");
            boolean test = false;
        } while (false);

        int a1 = 1___1;
        double b1 = 1.2;

        System.gc();

    }
}
