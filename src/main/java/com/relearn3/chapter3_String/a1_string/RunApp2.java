package com.relearn3.chapter3_String.a1_string;

public class RunApp2 {
    public static void main(String[] args) {
        String a = "al";
        String b = "a";

        System.out.printf("%50s %5s %7b\n", "Literal == String + String", "->", "ala" == a + b);
        System.out.printf("%50s %5s %7b\n", "Literal == String", "->", "al" == a);

        final String a2 = "al";
        final String b2 = "a";
        System.out.printf("%50s %5s %7b\n", "Literal == final String + final String", "->", "ala" == a2 + b2);

        System.out.println("StringBuilder");

        StringBuilder sb = new StringBuilder();
        sb.append("Ala ma kota");
        int id = sb.indexOf("ma");
        sb.delete(id, id + "ma ".length());
        sb.delete(4, 4);
        System.out.println("|" + sb + "|");

        System.out.println(sb.indexOf("ma"));
        System.out.println(sb.capacity());
        sb.replace(0, 1, "sdf");
    }
}
