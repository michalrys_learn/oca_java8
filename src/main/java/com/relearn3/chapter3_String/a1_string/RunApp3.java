package com.relearn3.chapter3_String.a1_string;

import java.util.Arrays;

public class RunApp3 {
    public static void main(String[] args) {
        //arrays

        args = new String[3];
        System.out.println(Arrays.toString(args));

        int[] a1, a2[];
        a1 = new int[]{1, 2, 3, 4, 5};
        a2 = new int[][]{{1, 2, 3, 4}, {}, new int[3]};

        for (int[] inter : a2) {
            System.out.println(Arrays.toString(inter));
        }


        System.out.println("-----\n");

        int[] a3[] = {{1, 2, 3, 4}, {}, new int[3]};
        int[] a4[];
        a4 = new int[][]{{1, 2, 3, 4}, {}, new int[3]};
//        for (int[] inter : a3) {
//            System.out.println(Arrays.toString(inter));
//        }
        for (int i = 0; i < a3.length; i++) {
            System.out.printf("%15s || %-5s\n", Arrays.toString(a3[i]), Arrays.toString(a4[i]));
        }
        System.out.println(a3[1].getClass());

        System.out.println("--------");
        char letters[] = new char[5];
        System.out.println(Arrays.toString(letters));
        System.out.println((int) letters[0]);
        System.out.println("|" + (char) 0 + "|");
        System.out.println("|" + (char) 1 + "|");
        System.out.println("\uFFFF");

        System.out.println(Integer.toBinaryString(0xFFFF));
        System.out.println(Integer.toOctalString(0xFFFF));
        System.out.println(Integer.toString(0xFFFF));
        System.out.println(Short.MAX_VALUE * 2);

        int x[] = {1};
        int y[] = {1};
        System.out.printf("X: %s -> %s\nY: %s -> %s\nx.equals(y) -> %s\nx == y -> %s\n"
                , x, Arrays.toString(x), y, Arrays.toString(y), x.equals(y), x == y);

        int[] d1, d2 = {1, 2, 3}, d3;

        int z[] = x;
        System.out.printf("z.equals(x) -> %s, z == x -> %s\n", z.equals(x), z == x);

        String[] text = {"10", "11", "100", "9", "93"};
        Arrays.sort(text);
        System.out.println(Arrays.toString(text));

    }
}
