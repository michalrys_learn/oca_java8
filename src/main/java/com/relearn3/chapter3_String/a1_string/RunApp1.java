package com.relearn3.chapter3_String.a1_string;

public class RunApp1 {
    public static void main(String[] args) {
        String a = "ala";
        final String b = new String("ala");

        System.out.println(a == b);

        String a3 = "ala3";
        final String b3 = "ala" + 3;
        final int i3 = 3;
        final String b3i = "ala";
        System.out.println(b == a3);
        System.out.println(b3i + i3 == a3);

        System.out.println("---");
        System.out.println(("ala" + "3") == a3);
        System.out.println((b3i + "3") == a3);
        System.out.println((a + "3") == a3);

    }
}
