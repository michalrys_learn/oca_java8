package com.relearn3.test1.overriding;

public class FirstYear extends Student{
    @Override
    public FirstYear getStudentAtMost() {
        return new FirstYear();
    }

    public static void main(String[] args) {
        Person person = new FirstYear();
        Student student = new FirstYear();
        System.out.println(student.getStudentAtMost());
    }
}
