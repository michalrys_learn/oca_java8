package com.relearn3.test1.overriding;

public class Student extends Person implements A, B, C{
    @Override
    public FirstYear get() {
        return null;
    }

    @Override
    public Student getPersonAtMost() {
        return new Student();
    }

    public Student getStudentAtMost() {
        return new Student();
    }

    public static void main(String[] args) {
        Student student = new Student();
        System.out.println(student.getPersonAtMost());

        Person studentPerson = new Student();
        System.out.println(studentPerson.getPersonAtMost());
    }
}
